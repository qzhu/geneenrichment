package seek;
import seek.TreeNode;
import java.util.LinkedList;
import java.util.Vector;
import java.util.Map;
import java.util.HashMap;

public class Cluster{

	float[][] d;
	int nelements;

	//temp variables
	int closest_pair_i;
	int closest_pair_j;
	float closest_distance;
	boolean started;

	public Cluster(float[][] d){
		this.d = d;
		this.nelements = d.length;
		closest_pair_i = 0;
		closest_pair_j = 0;
		closest_distance = 0;
		started = false;
	}

	private void find_closest_pair(int n){
		int i, j;
		float temp;
		float distance = d[1][0];
		int ip = 1;
		int jp = 0;
		for(i=1; i<n; i++){
			for(j=0; j<i; j++){
				temp = d[i][j];
				if(temp<distance){
					distance = temp;
					ip = i;
					jp = j;
				}
			}
		}
		closest_pair_i = ip;
		closest_pair_j = jp;
		closest_distance = distance;
	}

	private float max(float f1, float f2){
		if(f1>f2) return f1;
		else return f2;
	}

	public TreeNode[] HClusterMax(){
		int j, n;
		int[] clusterid = new int[nelements];
		TreeNode[] result = new TreeNode[nelements-1];

		if(started){
			System.err.println("Cluster instance already started. Please give a new Cluster instance.");
			return null;
		}

		started = true;
		for(j=0; j<nelements-1; j++){
			result[j] = new TreeNode();
		}
		for(j=0; j<nelements; j++)
			clusterid[j] = j;

		int js, is;
		for(n=nelements; n>1; n--){
			is = 1;
			js = 0;
			find_closest_pair(n);
			result[nelements-n].distance = closest_distance;
			is = closest_pair_i;
			js = closest_pair_j;

			for(j=0; j<js; j++)
				d[js][j] = max(d[is][j], d[js][j]);
			for(j=js+1; j<is; j++)
				d[j][js] = max(d[is][j], d[j][js]);
			for(j=is+1; j<n; j++)
				d[j][js] = max(d[j][is], d[j][js]);
			for(j=0; j<is; j++)
				d[is][j] = d[n-1][j];
			for(j=is+1; j<n-1; j++)
				d[j][is] = d[n-1][j];
			result[nelements-n].left = clusterid[is];
			result[nelements-n].right = clusterid[js];
			clusterid[js] = n-nelements-1;
			clusterid[is] = clusterid[n-1];
		}
		return result;
	}

	public TreeNode[] HClusterAvg(float distance){
		int j, n;
		int[] clusterid = new int[nelements];
		int[] number = new int[nelements];
		TreeNode[] result = new TreeNode[nelements-1];
		
		if(started){
			System.err.println("Cluster instance already started. Please give a new Cluster instance.");
			return null;
		}

		started = true;

		for(j=0; j<nelements-1; j++){
			result[j] = new TreeNode();
		}

		for(j=0; j<nelements; j++){
			clusterid[j] = j;
			number[j] = 1;
		}

		int js, is;
		for(n=nelements; n>1; n--){
			is = 1;
			js = 0;
			find_closest_pair(n);
			//System.out.printf("Closest distance: %.3f\n", closest_distance);
			if(closest_distance > distance){
				break;
			}
			result[nelements-n].distance = closest_distance;
			is = closest_pair_i;
			js = closest_pair_j;
			result[nelements-n].left = clusterid[is];
			result[nelements-n].right = clusterid[js];
			int sum = 2;
			for(j=0; j<js; j++)
				d[js][j] = (d[is][j] + d[js][j]) / sum;
			for(j=js+1; j<is; j++)
				d[j][js] = (d[is][j] + d[j][js]) / sum;
			for(j=is+1; j<n; j++)
				d[j][js] = (d[j][is] + d[j][js]) / sum;
			for(j=0; j<is; j++)
				d[is][j] = d[n-1][j];
			for(j=is+1; j<n-1; j++)
				d[j][is] = d[n-1][j];
			clusterid[js] = n-nelements-1;
			clusterid[is] = clusterid[n-1];
		}

		int num_initialized = nelements-n;
		TreeNode[] result2 = new TreeNode[num_initialized];
		for(j=0; j<num_initialized; j++){
			result2[j] = result[j];
		}
		return result2;
	}

	public TreeNode[] HClusterAvg(){
		int j, n;
		int[] clusterid = new int[nelements];
		int[] number = new int[nelements];
		TreeNode[] result = new TreeNode[nelements-1];

		if(started){
			System.err.println("Cluster instance already started. Please give a new Cluster instance.");
			return null;
		}

		started = true;

		for(j=0; j<nelements-1; j++){
			result[j] = new TreeNode();
		}

		for(j=0; j<nelements; j++){
			clusterid[j] = j;
			number[j] = 1;
		}

		int js, is;
		for(n=nelements; n>1; n--){
			is = 1;
			js = 0;
			find_closest_pair(n);
			//System.out.printf("Closest distance: %.3f\n", closest_distance);
			result[nelements-n].distance = closest_distance;
			is = closest_pair_i;
			js = closest_pair_j;
			result[nelements-n].left = clusterid[is];
			result[nelements-n].right = clusterid[js];

			int sum = number[is] + number[js];
			
			for(j=0; j<js; j++){
				d[js][j] = d[is][j]*number[is] + d[js][j]*number[js];
				d[js][j] /= sum;
			}

			for(j=js+1; j<is; j++){
				d[j][js] = d[is][j] * number[is] + d[j][js] * number[js];
				d[j][js] /= sum;
			}

			for(j=is+1; j<n; j++){
				d[j][js] = d[j][is] * number[is] + d[j][js]*number[js];
				d[j][js] /= sum;
			}

			for(j=0; j<is; j++)
				d[is][j] = d[n-1][j];

			for(j=is+1; j<n-1; j++)
				d[j][is] = d[n-1][j];

			clusterid[js] = n-nelements-1;
			clusterid[is] = clusterid[n-1];
		}

		return result;
	}

	public Map<Integer, Float> GetDepth(TreeNode[] t){
		LinkedList<Integer> li = new LinkedList<Integer>();
		li.addFirst(t.length-1);
		Map<Integer, Float> depth = new HashMap<Integer, Float>();
		depth.put(t.length*-1, 0f);
		depth = DepthRecursive(li, depth, t);
		return depth;
	}

	public Map<Integer,Float> DepthRecursive(LinkedList<Integer> li, 
		Map<Integer,Float> depth, TreeNode[] t){

		if(li.isEmpty()) return depth;
		int i = li.removeFirst();
		float d = depth.get((i+1)*-1);

		TreeNode tx = t[i];		
		int left = tx.left;
		if(left>=0){
			depth.put(left, d + tx.distance);
		}else{
			li.addFirst((left+1)*-1);
			depth.put(left, d + tx.distance);
			depth = DepthRecursive(li, depth, t);
		}
		int right = tx.right;
		if(right>=0){
			depth.put(right, d + tx.distance);
		}else{
			li.addFirst((right+1)*-1);
			depth.put(right, d + tx.distance);
			depth = DepthRecursive(li, depth, t);
		}
		return depth;
	}

	public Vector<Integer> Linearize(TreeNode[] t){
		LinkedList<Integer> li = new LinkedList<Integer>();
		li.addFirst(t.length-1);
		Vector<Integer> vi = new Vector<Integer>();
		vi = LinearizeRecursive(li, vi, t);
		return vi;
	}

	public Vector<Vector<Integer> > Bifurcate(TreeNode[] t){
		int first = t.length - 1;
		TreeNode tx = t[first];
		int left = tx.left;
		int right = tx.right;
		Vector<Integer> bifurcate1 = new Vector<Integer>();
		Vector<Integer> bifurcate2 = new Vector<Integer>();
		if(left>=0){
		}else{
			LinkedList<Integer> li = new LinkedList<Integer>();
			li.addFirst((left+1)*-1);
			bifurcate1 = LinearizeRecursive(li, bifurcate1, t);
		}
		if(right>=0){
		}else{
			LinkedList<Integer> li = new LinkedList<Integer>();
			li.addFirst((right+1)*-1);
			bifurcate2 = LinearizeRecursive(li, bifurcate2, t);
		}

		if(left>=0){
			bifurcate2.add(left);
		}
		if(right>=0){
			bifurcate1.add(right);
		}
		Vector<Vector<Integer> > ret = new Vector<Vector<Integer> >();
		ret.add(bifurcate1);
		ret.add(bifurcate2);
		return ret;
	}

	public Vector<Integer> LinearizeRecursive(LinkedList<Integer> li, Vector<Integer> r, TreeNode[] t){
		if(li.isEmpty()){
			return r;
		}
		int i = li.removeFirst();
		TreeNode tx = t[i];
		int left = tx.left;
		if(left>=0){
			r.add(left);
		}else{
			li.addFirst((left+1)*-1);
			r = LinearizeRecursive(li, r, t);
		}
		int right = tx.right;
		if(right>=0){
			r.add(right);
		}else{
			li.addFirst((right+1)*-1);
			r = LinearizeRecursive(li, r, t);
		}
		return r;
	}

	//GetComponents (designed to run after HClustAvg(distance))
	public Vector<Vector<Integer> > GetComponents(TreeNode[] t){
		boolean[] visitStatus = new boolean[t.length];
		for(int i=0; i<t.length; i++){
			visitStatus[i] = false;
		}

		Vector<Vector<Integer> > components = new Vector<Vector<Integer> >();

		while(true){
			//get a free node (unvisited)
			int unvisited = -1;
			for(int i=1; i<=t.length; i++){
				if(visitStatus[t.length-i]==false){
					unvisited = t.length-i;
					break;
				}
			}
			if(unvisited==-1){
				break;
			}
			//System.out.println("Got a unvisited node: " + unvisited);
			LinkedList<Integer> li = new LinkedList<Integer>();
			li.addFirst(unvisited);
			Vector<Integer> vi = new Vector<Integer>();
			vi = LinearizeRecursive(li, vi, t);
			//System.out.println("Finish linearizeRecursive");
			Vector<Integer> visited = new Vector<Integer>();
			li = new LinkedList<Integer>();
			li.addFirst(unvisited);
			visited = LinearizeRecursiveVisitedNodes(li, visited, t);
			//System.out.println("Finish linearizeRecursiveVisitedNode");
			for(int i=0; i<visited.size(); i++){
				visitStatus[visited.get(i)] = true;
			}
			components.add(vi);
		}
		return components;
	}

	//Returns the visited nodes for the current connected component, started by the variable li
	public Vector<Integer> LinearizeRecursiveVisitedNodes(LinkedList<Integer> li, Vector<Integer> r, TreeNode[] t){
		if(li.isEmpty()){
			return r;
		}
		int i = li.removeFirst();
		r.add(i);		
		TreeNode tx = t[i];
		int left = tx.left;
		if(left>=0){
		}else{
			li.addFirst((left+1)*-1);
			r = LinearizeRecursiveVisitedNodes(li, r, t);
		}
		int right = tx.right;
		if(right>=0){
		}else{
			li.addFirst((right+1)*-1);
			r = LinearizeRecursiveVisitedNodes(li, r, t);
		}
		return r;
	}
}
