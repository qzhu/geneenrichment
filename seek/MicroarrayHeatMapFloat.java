package seek;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.*;
import javax.swing.*;
import javax.imageio.*;
import java.io.*;
import java.net.*;
import java.nio.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.cli.HelpFormatter;


public class MicroarrayHeatMapFloat {
    final static int maxCharHeight = 15;
    final static int minFontSize = 15;

    final static Color fg = Color.black;
    final static Color bg = Color.white;
    final static Color red = Color.red;
    final static Color white = Color.white;
    final static Color green = Color.green;
    final static Color black = Color.black;

    BufferedImage image;

    final static BasicStroke stroke = new BasicStroke(2.0f);
    final static BasicStroke wideStroke = new BasicStroke(8.0f);

    final static float dash1[] = {10.0f};
    final static BasicStroke dashed = new BasicStroke(1.0f, 
		BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dash1, 0.0f);

    Dimension totalSize;
    FontMetrics fontMetrics;
    Random rnd;

	int hUnit;
	int vUnit;
	float[][] iMatrix;
	//float[][][] matrix;
	//float[][][] norm_matrix;
    
    int[][] color_red;
    int[][] color_green;
	//int size_x, size_y;
    //int[] size;
	int length_x, length_y;
   
 
    int numDatasetToShow;
    int numGeneToShow;
    int windowWidth;
    int windowHeight;
    String[] names;
    String[] description;
    Vector<String> dsets;
    Vector<String> genes;
    Map<String, String> map_genes;
    Map<String, String> map_datasets;
    
    final static int horizontalDatasetSpacing = 2;
    //final static int horizontalDatasetSpacing = 10;
    
    final static int verticalGeneSpacing = 0;
    final static int geneNameWidth = 0;
    final static int verticalUnitHeight = 15;

    private int calculateWidth(int s){
    	return s*calculateUnitWidth(s);
    }
    
    private int calculateUnitWidth(int s){
    	if(s<=10){
    		return 2;
    	}else if(s<=50){
    		return 2;
    	}else if(s<=200){
    		return 2;
    	}else if(s<=300){
    		return 2;
    	}else{
    		return 2;
    	}
    }

    public int getWindowWidth(){
    	return windowWidth;
    }
    
    public int getWindowHeight(){
    	return windowHeight;
    }
    
    public void save(String name){
		int i, j;
		File f = new File(name);
		try{
			ImageIO.write(image, "png", f);
		}catch(IOException e){	
			System.out.println("Error occurred!");
		}
    }
    
    public float[] getNormRow(float[] m){
    	int i;
    	float mean = 0;
    	float stdev = 0;
    	int num = 0;
    	for(i=0; i<m.length; i++){
    		if(m[i]>327f) continue;
    		num++;
    		mean+=m[i];
    	}
    	if(num==0){
    		float[] r= new float[m.length];
    		for(i=0; i<m.length; i++){
    			r[i] = 327.67f;
    		}
    		return r;
    	}
    	mean/=(float)num;
    	for(i=0; i<m.length; i++){
    		if(m[i]>327f) continue;
    		stdev+=(m[i] - mean)*(m[i] - mean);
    	}
    	stdev/=(float) num;
    	stdev = (float) Math.sqrt(stdev);
    	
    	float[] r= new float[m.length];
    	for(i=0; i<m.length; i++){
    		if(m[i]>327f){
    			r[i] = 327.67f;
    			continue;
    		}
    		r[i] = (m[i] - mean) / stdev;
    		//System.out.printf("%.2f ", r[i]);
    	}
    	return r;
    }
    
	//visualize 0/1 matrix
    public void init() {
		int i, j, k;
       
		color_red = new int[iMatrix.length][];
		color_green = new int[iMatrix.length][];

		for(i=0; i<iMatrix.length; i++){	//column
			color_red[i] = new int[iMatrix[i].length];
			color_green[i] = new int[iMatrix[i].length];
			for(j=0; j<iMatrix[i].length; j++){ //gene
				color_red[i][j] = 0;
				color_green[i][j] = 0;
			}
		}

		//maximum value of iMatrix is 1.0 
		//minimum value of iMatrix is -1.0 
        for(i=0; i<iMatrix.length; i++){
        	for(j=0; j<iMatrix[i].length; j++){
				if(iMatrix[i][j]>=0){
					color_red[i][j] = (int) (iMatrix[i][j] * 255.0);
				}else{
					color_green[i][j] = (int) (-1.0 * iMatrix[i][j] * 255.0);
				}
			}
		}

		/*names = new String[genes.size()];
        for(i=0; i<names.length; i++){
        	names[i] = new String(map_genes.get(genes.get(i)));
        }*/
        
       /* int STRING_MAX_LENGTH = 200;
        description = new String[numDatasetToShow];
        
        for(k=0; k<numDatasetToShow; k++){
        	//String stem = getStem(dsets.get(k));
			String stem = ReadScore.getDatasetID(dsets.get(k));
        	String descr = map_datasets.get(stem);
        	if(descr.length()>STRING_MAX_LENGTH){
        		descr = descr.substring(0, 200) + "...";
        	}
        	description[k] = "#" + (k+1) + "  " + stem + ": " + descr;
        }*/
        
        //new
        //calculateSize();
		length_y = iMatrix[0].length; //number of rows
		length_x = iMatrix.length; //number of columns

		hUnit = 1;
		vUnit = 1;

		int size = iMatrix.length;
		int size_x = hUnit * size + 2 * horizontalDatasetSpacing;
		int size_y = vUnit * iMatrix[0].length;

		image = new BufferedImage(size_x, size_y, BufferedImage.TYPE_INT_RGB);
        
    }

    FontMetrics pickFont(Graphics2D g2, String longString, int xSpace) {
        boolean fontFits = false;
        Font font = g2.getFont();
        FontMetrics fontMetrics = g2.getFontMetrics();
        int size = font.getSize();
        String name = font.getName();
        int style = font.getStyle();
        while ( !fontFits ) {
            if ( (fontMetrics.getHeight() <= maxCharHeight)
                 && (fontMetrics.stringWidth(longString) <= xSpace) ) {
                fontFits = true;
            } else {
                if ( size <= minFontSize ) {
                    fontFits = true;
                } else {
                    g2.setFont(font = new Font(name, style, --size));
                    fontMetrics = g2.getFontMetrics();
                }
            }
        }
        return fontMetrics;
    }

    public void paint() {
		int si, sj;
		int i, j;

		/*int[] unitWidth = new int[size.length];
        for(i=0; i<size.length; i++){ //dataset
        	unitWidth[i] = calculateUnitWidth(size[i]);
        }

		for(si=0; si<numDatasetToShow; si++){

		int size_x = calculateWidth(size[si])  + 2*horizontalDatasetSpacing;
		int size_y = verticalUnitHeight;

		for(sj=0; sj<numGeneToShow; sj++){
		*/
        Graphics2D g2 = image.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
        	RenderingHints.VALUE_ANTIALIAS_ON);

		//int hUnit = 2;
		//int vUnit = 1;
		int size_x = hUnit * length_x + 2 * horizontalDatasetSpacing;
		int size_y = vUnit * length_y;
		//int size_x = hUnit * length_x;
		//int size_y = vUnit * length_y;

        g2.setPaint(bg);
        g2.fill(new Rectangle2D.Double(0, 0, size_x, size_y));
        g2.setPaint(fg);

		int y = 0;
		for(i=0; i<length_y; i++){
			int x = horizontalDatasetSpacing;
	        for(j=0; j<length_x; j++){ //column in dataset
    	    	if(iMatrix[j][i]==0){
	        		g2.setPaint(new Color(0, 0, 0));
    	    	}else{
        			g2.setPaint(new Color(
        				color_red[j][i], color_green[j][i], 0));
        		}
	        	g2.fill(new Rectangle2D.Double(x, y, hUnit, 
	        		vUnit));
				x+=hUnit;
	        }
			y+=vUnit;
		}

    }
   
	public String[] getGeneNames(){
		return names;
	}
 
    /*public String getStem(String s){
    	int i = s.indexOf("_");
    	int j = s.indexOf(".");
    	if(i!=-1){
    		return s.substring(0, i);
    	}
    	return s.substring(0, j);
    }*/
    
    public Vector<String> getDatasets(){
    	return dsets;
    }
    
    public Vector<String> getGenes(){
    	return genes;
    }
    
    
    public void read(String query, String gm, String dm, int numD, int numG) 
    	throws IOException{
    		
    	Scanner sc = null;
    	dsets = new Vector<String>();
    	genes = new Vector<String>();
    	this.numDatasetToShow = numD;
    	this.numGeneToShow = numG;
    	
    	try{
    		sc = new Scanner(new BufferedReader(new FileReader(query)));
    		String s1 = sc.nextLine();
    		StringTokenizer st = new StringTokenizer(s1);
    		while(st.hasMoreTokens() && dsets.size()<numD)
    			dsets.add(st.nextToken() + ".bin");
    		s1 = sc.nextLine();
    		st = new StringTokenizer(s1);
    		while(st.hasMoreTokens() && genes.size()<numG)
    			genes.add(st.nextToken());
    	}finally{
    		sc.close();
    	}
    	
    	map_genes = new HashMap<String, String>();
    	try{
    		sc = new Scanner(new BufferedReader(new FileReader(gm)));
    		while(sc.hasNext()){
    			String s1 = sc.nextLine();
    			StringTokenizer st = new StringTokenizer(s1, "\t");
    			String s2 = st.nextToken();
    			String s3 = st.nextToken();
    			map_genes.put(s2, s3);
    		}
    		//System.out.println(s2 + " " + s3);
    	}finally{
    		sc.close();
    	}
    		
    	map_datasets = new HashMap<String, String>();
    	try{
    		sc = new Scanner(new BufferedReader(new FileReader(dm)));
    		while(sc.hasNext()){
    			String s1 = sc.nextLine();
    			StringTokenizer st = new StringTokenizer(s1, "\t");
    			String s2 = st.nextToken();
    			String s3 = st.nextToken();
    			map_datasets.put(s2, s3);
    		}
    	}finally{
    		sc.close();
    	}
    }

	public void Load(String matrixFile, boolean containsHeader) 
	throws IOException{
		Scanner sc = null;
		Vector<Vector<Float> > vi = new Vector<Vector<Float> >();
		//int[][] mm = null;		
		
		Vector<String> vecHeader = new Vector<String>();
		Vector<String> vecGenes = new Vector<String>();

		try{
			sc = new Scanner(new BufferedReader(new FileReader(matrixFile)));
			if(containsHeader){ //READ column header first
				String header = sc.nextLine();
				StringTokenizer st_header = new StringTokenizer(header, "\t");
				st_header.nextToken();
				while(st_header.hasMoreTokens()){
					vecHeader.add(st_header.nextToken());
				}
			}

			while(sc.hasNext()){
				String s1 = sc.nextLine();
				StringTokenizer st = new StringTokenizer(s1, "\t");
				if(containsHeader){ //READ gene header first
					String g = st.nextToken();
					vecGenes.add(g);
				}
				Vector<Float> vs = new Vector<Float>();
				while(st.hasMoreTokens()){
					vs.add(Float.parseFloat(st.nextToken()));
				}
				vi.add(vs);
			}

			int nG = vi.size();
			int nA = vi.get(0).size();

			iMatrix = new float[nA][nG];
			for(int i=0; i<nA; i++){
				for(int j=0; j<nG; j++){
					iMatrix[i][j] = vi.get(j).get(i);
				}
			}

		}catch(IOException e){
		}


	}   

	public static void main(String[] args) throws IOException{
		Options opt = new Options();
		opt.addOption("i", true, "Input file");
		opt.addOption("o", true, "Output file (png)");
		opt.addOption("e", false, "Input file contains header (default: false)");

		opt.getOption("i").setRequired(true);
		opt.getOption("o").setRequired(true);
		opt.getOption("e").setRequired(false);

		boolean containsHeader = false;
		CommandLineParser parser = new PosixParser();
		CommandLine cmd = null;
		try{
			cmd = parser.parse(opt, args);
		}catch(Exception e){
			System.out.println("Error parsing command args!");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("MicroarrayHeatMap", opt);
			return;
		}

		if(cmd.hasOption("e")){
			containsHeader = true;
		}

		String src = cmd.getOptionValue("i");
		String dest = cmd.getOptionValue("o");

		System.out.println("Source file " + src);
		System.out.println("Destination file " + dest);

		try{
			MicroarrayHeatMapFloat mh = new MicroarrayHeatMapFloat();
			mh.Load(src, containsHeader); 
			mh.init();
			mh.paint();
			mh.save(dest);
		}catch(IOException e){

		}
	}

}
