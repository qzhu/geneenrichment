package seek;

import java.io.*;
import java.util.*;

public class GeneSet{


	public static Map<String, Vector<String> > readGeneEntrezMapping(String file){
		Map<String, Vector<String> > m = 
			new HashMap<String, Vector<String> >();

		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			String s = null;
			while((s=in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(s, "\t");
				String entrez_id = st.nextToken();
				String gene_name = st.nextToken();
				Vector<String> vs;	
				if(m.containsKey(gene_name)){
					vs = m.get(gene_name);
				}else{
					vs = new Vector<String>();
				}
				vs.add(entrez_id);
				m.put(gene_name, vs);
			}
		}catch(IOException e){
			System.out.println("Error opening file " + file);
		}
		return m;
	}

	
	public static Map<String, Vector<String> > convertGeneEntrez(
		Map<String, Vector<String> > ma){

		Map<String, Vector<String> > m =  new HashMap<String, Vector<String> >();
		for(String gene_name : ma.keySet()){
			for(String ent : ma.get(gene_name)){
				Vector<String> vs;
				if(m.containsKey(ent)){
					vs = m.get(ent);
				}else{
					vs = new Vector<String>();
				}
				vs.add(gene_name);
				m.put(ent, vs);
			}
		}
		return m;
	}


	public static Map<String, Vector<String> > readGeneSet(
		String file){

		Map<String, Vector<String> > m = 
			new HashMap<String, Vector<String> >();

		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			String s = null;
			String go = "";
			while((s=in.readLine())!=null){
				go = new String(s);
				s = in.readLine();
				StringTokenizer st = new StringTokenizer(s, " ");
				Vector<String> vs = new Vector<String>();
				while(st.hasMoreTokens()){
					vs.add(st.nextToken());
				}
				//if(vs.size()<5){
				/*if(vs.size()<10 || vs.size()>300){
					continue;
				}*/
				Collections.sort(vs);
				m.put(go, vs);
			}
		}catch(IOException e){
			System.out.println("Error opening file " + file);
		}

		return m;
	}

	public static Map<String, Vector<Integer> > convertGeneSetToInteger(
		Map<String, Vector<String> > ms, Map<String, Integer> m){

		Map<String, Vector<Integer> > mi = new HashMap<String, Vector<Integer> >();
		for(String s : ms.keySet()){
			Vector<Integer> vi = new Vector<Integer>();
			for(String t : ms.get(s)){
				if(m.containsKey(t)) vi.add(m.get(t));
			}
			Collections.sort(vi);
			mi.put(s, vi);
		}

		return mi;
	}


	public static Map<String, Integer> convertGeneNameToInteger(
		Set<String> s){

		Map<String, Integer> m = new HashMap<String, Integer>();
		int i = 0;
		for(String t : s){
			m.put(t, i);
			i++;
		}
		return m;
	}

	//supposed that v1 and v2 are each sorted
	public static int getGeneSetOverlap(Vector<Integer> v1, Vector<Integer> v2){
		int c = 0;
		int first1 = 0;
		int last1 = v1.size();
		int first2 = 0;
		int last2 = v2.size();
		while(first1!=last1 && first2!=last2){
			if(v1.get(first1)<v2.get(first2)) first1++;
			else if(v2.get(first2)<v1.get(first1)) first2++;
			else{ c++; first1++; first2++;}
		}
		return c;
	}

	public static Vector<Integer> getGeneSetOverlapGenes(Vector<Integer> v1, Vector<Integer> v2){
		int first1 = 0;
		int last1 = v1.size();
		int first2 = 0;
		int last2 = v2.size();
		Vector<Integer> vi = new Vector<Integer>();
		while(first1!=last1 && first2!=last2){
			if(v1.get(first1)<v2.get(first2)) first1++;
			else if(v2.get(first2)<v1.get(first1)) first2++;
			else{ vi.add(v1.get(first1)); first1++; first2++;}
		}
		return vi;
	}
}
