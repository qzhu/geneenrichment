package seek;
import java.nio.*;
import java.net.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.math3.distribution.HypergeometricDistribution;
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;
import seek.GeneSet;
import seek.Pair;
import java.text.*;

public class GeneEnrichment extends HttpServlet {

	public static boolean doCommand(String cmd) throws Exception{
		try{
			Process p = Runtime.getRuntime().exec(cmd);
			BufferedReader in=new BufferedReader(new 
				InputStreamReader(p.getInputStream())); 
			BufferedReader err=new BufferedReader(new 
				InputStreamReader(p.getErrorStream())); 
			String s = null;
			System.out.println("Stdout:");
			while ((s=in.readLine())!=null){
				System.out.println(s);
			}
			System.out.println("Stderr:");
			while ((s=err.readLine())!=null){
				System.out.println(s);
			}
			in.close();
			err.close();
			//p.waitFor();
		}catch(Exception e){
			System.out.println("Error");
			return false;
		}
		return true;
	}

	public static boolean doCommand(String[] cmd) throws Exception{
		try{
			Process p = Runtime.getRuntime().exec(cmd);
			BufferedReader in=new BufferedReader(new 
				InputStreamReader(p.getInputStream())); 
			BufferedReader err=new BufferedReader(new 
				InputStreamReader(p.getErrorStream())); 
			String s = null;
			System.out.println("Stdout:");
			while ((s=in.readLine())!=null){
				System.out.println(s);
			}
			System.out.println("Stderr:");
			while ((s=err.readLine())!=null){
				System.out.println(s);
			}
			in.close();
			err.close();
		}catch(Exception e){
			System.out.println("Error");
			return false;
		}
		return true;
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {

		String sessionID = req.getParameter("sessionID");
		int top = Integer.parseInt(req.getParameter("top"));
		String goldstd = req.getParameter("goldstd");
		String toShowGenes = req.getParameter("show_overlap"); //used in combination of above

		boolean include_search_query = Boolean.parseBoolean(req.getParameter("include_query"));

		String showOverlapOnly = req.getParameter("overlap_only"); //used with term parameter		
		String doTerm = req.getParameter("term");
		boolean filter_by_pval = Boolean.parseBoolean(req.getParameter("filter_by_pval"));
		float pvalCutoff = Float.parseFloat(req.getParameter("pval_cutoff"));

		boolean showGenes;
		if(toShowGenes.equals("y"))
			showGenes = true;
		else
			showGenes = false;

		boolean overlapOnly;
		if(showOverlapOnly.equals("y"))
			overlapOnly = true;
		else
			overlapOnly = false;

		String strMinSize = req.getParameter("min_size");
		String strMaxSize = req.getParameter("max_size");
		int minSize = 10;
		int maxSize = 99999;
		if(strMinSize!=null){
			minSize = Integer.parseInt(strMinSize);
		}
		if(strMaxSize!=null){
			maxSize = Integer.parseInt(strMaxSize);
		}

		File tempDir = (File) getServletContext().
			getAttribute( "javax.servlet.context.tempdir" );

		String s1 = tempDir + "/" + "gene_entrez_symbol.txt";
		Map<String, Vector<String> > mge = GeneSet.readGeneEntrezMapping(s1);
		Map<String, Vector<String> > meg = GeneSet.convertGeneEntrez(mge);
		Set<String> all_entrez = meg.keySet();
		Map<String, Integer> mentrez = 
			GeneSet.convertGeneNameToInteger(all_entrez);
		Map<Integer, String> mreverse = new HashMap<Integer, String>();
		for(String s : mentrez.keySet())
			mreverse.put(mentrez.get(s), s);

		//read gold standard gene set
		String s2 = "";
		if(goldstd.equals("msigdb_curated")){
			s2 = tempDir + "/" + "msigdb.curated.gene.set.oct11";
		}else if(goldstd.equals("msigdb_comput")){
			s2 = tempDir + "/" + "msigdb.computational.gene.set.oct11";
		}else if(goldstd.equals("msigdb_chr")){
			s2 = tempDir + "/" + "msigdb.positional.gene.set.oct11";
		}else if(goldstd.equals("netcompare")){
			s2 = tempDir + "/" + 
				"gene_ontology_combined_experimental_netcompare.sept28";
		}else if(goldstd.equals("original")){
			s2 = tempDir + "/" + "all_gene_ontology_annot.apr1.entrez";
		}else if(goldstd.equals("withIEA")){
			s2 = tempDir + "/" + "all_IEA_annot.entrez";
		}else if(goldstd.equals("msigdb_miRNA")){
			s2 = tempDir + "/" + "msigdb.separate.miRNA.gene.set.oct11";
		}else if(goldstd.equals("targetscan")){
			s2 = tempDir + "/" + "targetscan.conserved.entrez";
		}else if(goldstd.equals("targetscan_family")){
			s2 = tempDir + "/" + "targetscan.family.conserved.entrez";
		}else if(goldstd.equals("msigdb_go_bp")){
			s2 = tempDir + "/" + "msigdb.separate.bp.gene.set.oct11";
		}else if(goldstd.equals("msigdb_go_cc")){
			s2 = tempDir + "/" + "msigdb.separate.cc.gene.set.oct11";
		}else if(goldstd.equals("msigdb_go_mf")){
			s2 = tempDir + "/" + "msigdb.separate.mf.gene.set.oct11";
		}else if(goldstd.equals("msigdb_biocarta")){
			s2 = tempDir + "/" + "msigdb.separate.biocarta.gene.set.oct11";
		}else if(goldstd.equals("msigdb_kegg")){
			s2 = tempDir + "/" + "msigdb.separate.kegg.gene.set.oct11";
		}else if(goldstd.equals("msigdb_reactome")){
			s2 = tempDir + "/" + "msigdb.separate.reactome.gene.set.oct11";
		}else if(goldstd.equals("msigdb_cgp")){
			s2 = tempDir + "/" + "msigdb.separate.cgp.gene.set.oct11";
		}else if(goldstd.equals("withIEA_bp")){
			s2 = tempDir + "/" + "iea_bp_human.annot";
		}else if(goldstd.equals("experimental_bp")){
			s2 = tempDir + "/" + "experimental_bp_human.annot";
		}else if(goldstd.equals("withIEA_cp")){
			s2 = tempDir + "/" + "iea_cp_human.annot";
		}else if(goldstd.equals("experimental_cp")){
			s2 = tempDir + "/" + "experimental_cp_human.annot";
		}else if(goldstd.equals("withIEA_mf")){
			s2 = tempDir + "/" + "iea_mf_human.annot";
		}else if(goldstd.equals("experimental_mf")){
			s2 = tempDir + "/" + "experimental_mf_human.annot";
		}else if(goldstd.equals("kegg")){
			s2 = tempDir + "/" + "kegg_human.annot";
		}

		Map<String, Vector<String> > gold_std = GeneSet.readGeneSet(s2, minSize, maxSize);
		Map<String, Vector<Integer> > gold_std_int = 
			GeneSet.convertGeneSetToInteger(gold_std, mentrez);

		int population = 18000; //temporary
		Set<Integer> all_genes = new HashSet<Integer>();
		Set<String> all_gene_names = new HashSet<String>();
		for(String term : gold_std_int.keySet()){
			for(Integer gs : gold_std_int.get(term))
				all_genes.add(gs);
			for(String gg : gold_std.get(term))
				all_gene_names.add(gg);
		}
		population = all_genes.size();

		//reading gene names sorted by gene scores
		File tempFile = new File(tempDir + "/" + sessionID + "_gscore");
		String name_mapping = tempDir + "/gene_id.map";
		Map<String, Integer> mm = ReadScore.readGeneMapping(name_mapping);		
		float[] sc = ReadScore.ReadScoreBinary(tempFile.getAbsolutePath());
		String pval_path = tempDir + "/" + sessionID + "_pval";
		float[] sc_pval = ReadScore.ReadScoreBinary(pval_path);

		//filter by pval
		if(filter_by_pval){
			for(int i=0; i<sc.length; i++){
				if(sc_pval[i]>pvalCutoff){
					sc[i] = -320.0f;
				}
			}
		}

		Vector<Pair> valid = ReadScore.SortScores(mm, sc);

		int totAvailable = 0;	
		for(int i=0; i<valid.size(); i++){
			if(valid.get(i).val==0f) break;
			totAvailable++;
		}
		if(totAvailable<top){
			top = totAvailable;
		}

		Vector<Integer> query = new Vector<Integer>();
		if(include_search_query){
			Vector<String> searchQuery = 
				ReadScore.readQuery(tempDir+"/"+sessionID+"_query");
			for(int i=0; i<searchQuery.size(); i++){
				if(all_gene_names.contains(searchQuery.get(i)) &&
				mentrez.containsKey(searchQuery.get(i))){
					query.add(mentrez.get(searchQuery.get(i)));
				}
			}
		}

		//get top X genes (where X is a parameter from command line)
		for(int i=0; i<top; i++){
			if(all_gene_names.contains(valid.get(i).term) &&
			mentrez.containsKey(valid.get(i).term)){
				query.add(mentrez.get(valid.get(i).term));
				//System.out.println(valid.get(i).term + "\t" + mentrez.get(valid.get(i).term));
			}
		}
		Collections.sort(query);

		if(overlapOnly){
			String term = new String(doTerm);
			Vector<Integer> overlap = GeneSet.getGeneSetOverlapGenes(
				query, gold_std_int.get(term));
			Set<String> si = new HashSet<String>();
			for(Integer ii : overlap)
				for(String ss : meg.get(mreverse.get(ii)))
					si.add(ss);
			Vector<String> vi = new Vector(si);
			Collections.sort(vi);
			String svi = StringUtils.join(vi, " ") + "\n";
			res.setContentType("text/plain");
			res.setCharacterEncoding("UTF-8");
			res.getWriter().write(svi);
			System.out.println(svi);
			return ;
		}

		Vector<Pair> vp = new Vector<Pair>();
		Map<String, Vector<Integer> > map_overlap = new 
			HashMap<String, Vector<Integer> >();
		for(String term : gold_std_int.keySet()){
			Vector<Integer> overlap = GeneSet.getGeneSetOverlapGenes(
				query, gold_std_int.get(term));
			//System.out.println(term);
			//if(overlap.size()<=1) continue;
			map_overlap.put(term, new Vector<Integer>(overlap));
			int successes = gold_std_int.get(term).size();
			HypergeometricDistribution hyp = new 
				HypergeometricDistribution(population, successes, query.size());
			double prob = hyp.upperCumulativeProbability(overlap.size());
			if(prob>=1.0){
				prob = 0.999999999;
			}	
			Pair pp = new Pair(term, prob, -1);
			vp.add(pp);
		}	
		Collections.sort(vp);

		String st = "/tmp/" + tempFile.getName() + ".pval";
		PrintWriter out1 = null;
		try{
			out1 = new PrintWriter(new FileWriter(st));
			for(int i=0; i<vp.size(); i++){
				Pair pi = vp.get(i);
				out1.println(pi.val);
			}
		}catch(Exception e){
			System.out.println("BAD IO 1");
			return ;
		}finally{
			if(out1 !=null) out1.close();
		}
		
		String sv = "/tmp/" + tempFile.getName() + ".qval";
		try{
			if(!doCommand(tempDir + "/qval.R " + st + " " + sv)){
				System.out.println("Error!");
				return ;
			}
		}catch(Exception e){
			System.out.println("exception happened - here's what I know: ");
			e.printStackTrace();
		}

		Vector<Float> qval = new Vector<Float>();
		BufferedReader in = null;
		try{
			in = new BufferedReader(new FileReader(sv));
			String s = null;
			while((s=in.readLine())!=null){
				qval.add(Float.parseFloat(s));
			}
		}catch(IOException e){
			System.out.println("BAD IO 2");
			return ;
		}finally{
			if(in!=null) in.close();
		}

		res.setContentType("text/plain");
		res.setCharacterEncoding("UTF-8");
		DecimalFormat myFormat = new DecimalFormat("0.##E0");

		double prev_value = 0;
		Map<Set<String>, Vector<String> > output = 
			new HashMap<Set<String>, Vector<String> >();
		Vector<Set<String> > order = new Vector<Set<String> >();

		//output results to client
		for(int i=0; i<vp.size(); i++){
			Pair pi = vp.get(i);
			double b_value = pi.val * gold_std_int.get(pi.term).size() / (double) (i+1);
			if(b_value>1){
				b_value = 1.0;
			}
			if(b_value<prev_value){
				b_value = prev_value;
			}
			prev_value = b_value;

			//sx stores the list of overlapped genes in gene symbol
			Set<String> sx = new HashSet<String>();
			for(Integer ii : map_overlap.get(pi.term)){
				for(String ss : meg.get(mreverse.get(ii))){
					sx.add(ss);
				}				
			}
			
			if(goldstd.equals("targetscan") || goldstd.equals("targetscan_family")){

				if(map_overlap.get(pi.term).size()<=1) continue;
				if(pi.val>=0.05 || qval.get(i)>0.5) continue;

				String decp = myFormat.format(b_value);
				String decq = myFormat.format(qval.get(i));

				String str = pi.term + "\t" + decp + "\t" + decq + "\t" + 
					gold_std_int.get(pi.term).size() + "\t" + query.size() + 
					"\t" + map_overlap.get(pi.term).size();

				//duplicate overlapping terms detection
				Vector<String> vs = output.get(sx);
				if(vs==null){
					vs = new Vector<String>();
					order.add(sx);
				}
				vs.add(str);
				output.put(sx, vs);

				//res.getWriter().write(str + "\n");
			}else{
				if(pi.val>=0.05 || qval.get(i)>0.25) break;
				
				String decp = myFormat.format(b_value);
				String decq = myFormat.format(qval.get(i));
				
				String str = pi.term + "\t" + decp + "\t" + decq + "\t" + 
					gold_std_int.get(pi.term).size() + "\t" + query.size() + 
					"\t" + map_overlap.get(pi.term).size();

				//duplicate overlapping terms detection
				Vector<String> vs = output.get(sx);
				if(vs==null){
					vs = new Vector<String>();
					order.add(sx);
				}
				vs.add(str);
				output.put(sx, vs);

				//res.getWriter().write(str + "\n");
			}
			/*if(showGenes){
				for(Integer ii : map_overlap.get(pi.term))
					for(String ss : meg.get(mreverse.get(ii))){
						res.getWriter().write(ss + " ");
					}				
				res.getWriter().write("\n");
			}*/
		}

		//output unique terms
		for(int i=0; i<order.size(); i++){
			Set<String> sx = order.get(i);
			Vector<String> cp = output.get(sx);
			if(goldstd.equals("targetscan") || goldstd.equals("targetscan_family")){
				for(int j=0; j<cp.size(); j++){
					res.getWriter().write(cp.get(j) + "\n");
					if(showGenes){
						for(String su : sx){
							res.getWriter().write(su + " ");
						}
						res.getWriter().write("\n");
					}
				}
			}
			else{
				res.getWriter().write(cp.get(0) + "\n");
				if(showGenes){
					for(String su : sx){
						res.getWriter().write(su + " ");
					}
					res.getWriter().write("\n");
				}
			}
		}

	}


}
