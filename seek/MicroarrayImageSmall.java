package seek;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.*;
import javax.swing.*;
import javax.imageio.*;
import java.io.*;
import java.net.*;
import java.nio.*;

public class MicroarrayImageSmall {
    final static int maxCharHeight = 15;
    final static int minFontSize = 15;

    final static Color fg = Color.black;
    final static Color bg = Color.white;
    final static Color red = Color.red;
    final static Color white = Color.white;
    final static Color green = Color.green;
    final static Color black = Color.black;

    BufferedImage[][] image;

    final static BasicStroke stroke = new BasicStroke(2.0f);
    final static BasicStroke wideStroke = new BasicStroke(8.0f);

    final static float dash1[] = {10.0f};
    final static BasicStroke dashed = new BasicStroke(1.0f, 
		BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dash1, 0.0f);

    Dimension totalSize;
    FontMetrics fontMetrics;
    Random rnd;
    float[][][] matrix;
    float[][][] norm_matrix;
    
    int[][][] color_red;
    int[][][] color_green;
    int[] size;
    
    int numDatasetToShow;
    int numGeneToShow;
    int windowWidth;
    int windowHeight;
    String[] names;
    String[] description;
    Vector<String> dsets;
    Vector<String> genes;
    Map<String, String> map_genes;
    Map<String, String> map_datasets;
    
    final static int horizontalDatasetSpacing = 2;
    //final static int horizontalDatasetSpacing = 10;
    
    final static int verticalGeneSpacing = 0;
    final static int geneNameWidth = 0;
    final static int verticalUnitHeight = 15;

    private int calculateWidth(int s){
    	return s*calculateUnitWidth(s);
    }
    
    private int calculateUnitWidth(int s){
    	if(s<=10){
    		return 2;
    	}else if(s<=50){
    		return 2;
    	}else if(s<=200){
    		return 2;
    	}else if(s<=300){
    		return 2;
    	}else{
    		return 2;
    	}
    }

    public void calculateSize(){
    	int x = 5 + geneNameWidth;
    	int y = 0;
    	
    	for(int i=0; i<numDatasetToShow; i++){
    		x+=calculateWidth(size[i]) + horizontalDatasetSpacing;
    	}
    	x+=10;
    	
    	y += verticalUnitHeight*numGeneToShow;
    	//y += 30;
    	//y += numDatasetToShow * verticalUnitHeight;
    	
    	windowWidth = x;
    	windowHeight = y;
    }
    
    
    public int getWindowWidth(){
    	return windowWidth;
    }
    
    public int getWindowHeight(){
    	return windowHeight;
    }
    
    public void save(String name){
		int i, j;
		for(i=0; i<numDatasetToShow; i++){
			for(j=0; j<numGeneToShow; j++){
				File f = new File(name + "_D" + i + "_G" + j + ".png");
				try{
					ImageIO.write(image[i][j], "png", f);
				}catch(IOException e){	
				}
			}
		}
    }
    
    public float[] getNormRow(float[] m){
    	int i;
    	float mean = 0;
    	float stdev = 0;
    	int num = 0;
    	for(i=0; i<m.length; i++){
    		if(m[i]>327f) continue;
    		num++;
    		mean+=m[i];
    	}
    	if(num==0){
    		float[] r= new float[m.length];
    		for(i=0; i<m.length; i++){
    			r[i] = 327.67f;
    		}
    		return r;
    	}
    	mean/=(float)num;
    	for(i=0; i<m.length; i++){
    		if(m[i]>327f) continue;
    		stdev+=(m[i] - mean)*(m[i] - mean);
    	}
    	stdev/=(float) num;
    	stdev = (float) Math.sqrt(stdev);
    	
    	float[] r= new float[m.length];
    	for(i=0; i<m.length; i++){
    		if(m[i]>327f){
    			r[i] = 327.67f;
    			continue;
    		}
    		r[i] = (m[i] - mean) / stdev;
    		//System.out.printf("%.2f ", r[i]);
    	}
    	return r;
    }
    

    public void init(float[][][] mat) {
        rnd = new Random();
        
        matrix = mat;
        int i, j, k;
       
        color_red = new int[matrix.length][][];
        color_green = new int[matrix.length][][];
        norm_matrix = new float[matrix.length][][];
        size = new int[matrix.length];
        
        for(i=0; i<matrix.length; i++){ //dataset
        	norm_matrix[i] = new float[matrix[i].length][];
        	color_red[i] = new int[matrix[i].length][];
        	color_green[i] = new int[matrix[i].length][];
        	size[i] = matrix[i].length;  //number of column
        	for(j=0; j<matrix[i].length; j++){ 
        		color_red[i][j] = new int[matrix[i][j].length];
        		color_green[i][j] = new int[matrix[i][j].length];
        		norm_matrix[i][j] = new float[matrix[i][j].length];
        	}

        	for(j=0; j<matrix[i][0].length; j++){ //number of genes
        		float[] mm = new float[size[i]];
        		for(k=0; k<size[i]; k++){
        			mm[k] = matrix[i][k][j];
        		}
        		float[] xm = getNormRow(mm);
        		for(k=0; k<size[i]; k++){
        			norm_matrix[i][k][j] = xm[k];
        		}
        	}
        }
         
        names = new String[genes.size()];
        float clip = 3.0f;
        for(i=0; i<matrix.length; i++){
        	for(j=0; j<matrix[i].length; j++){
        		for(k=0; k<matrix[i][j].length; k++){        	
        			//matrix[i][j][k] = rnd.nextFloat()*10.0f;
        			color_red[i][j][k] = 0;
        			color_green[i][j][k] = 0;
        			if(norm_matrix[i][j][k]>327.0f){
        				color_green[i][j][k] = 128;
        				color_red[i][j][k]= 128;
        				continue;
        			}
        			if(norm_matrix[i][j][k]>0.0f){
        				if(norm_matrix[i][j][k]>clip){
        					color_green[i][j][k] = 255;
        				}else{
        					color_green[i][j][k] = (int) (1.0f*norm_matrix[i][j][k] / clip * 255.0f);
        				}
        			}else if(norm_matrix[i][j][k]<0.0f){
        				if(norm_matrix[i][j][k]<-1.0f*clip){
        					color_red[i][j][k] = 255;
        				}else{
        					color_red[i][j][k] = (int) (-1.0f*norm_matrix[i][j][k] / clip * 255.0f);
        				}
        			}
        		}
        	}
        }
        
        for(i=0; i<names.length; i++){
        	names[i] = new String(map_genes.get(genes.get(i)));
        }
        
        int STRING_MAX_LENGTH = 200;
        description = new String[numDatasetToShow];
        
        for(k=0; k<numDatasetToShow; k++){
        	//String stem = getStem(dsets.get(k));
			String stem = ReadScore.getDatasetID(dsets.get(k));
        	String descr = map_datasets.get(stem);
        	if(descr.length()>STRING_MAX_LENGTH){
        		descr = descr.substring(0, 200) + "...";
        	}
        	description[k] = "#" + (k+1) + "  " + stem + ": " + descr;
        }
        
        //new
        //calculateSize();
		image = new BufferedImage[numDatasetToShow][numGeneToShow];

		for(i=0; i<numDatasetToShow; i++){
			int size_x = calculateWidth(size[i])  + 2*horizontalDatasetSpacing;
			int size_y = verticalUnitHeight;
			for(k=0; k<numGeneToShow; k++){
        		image[i][k] = new BufferedImage(size_x, size_y, BufferedImage.TYPE_INT_RGB);
        	}
		}
        
    }

    FontMetrics pickFont(Graphics2D g2, String longString, int xSpace) {
        boolean fontFits = false;
        Font font = g2.getFont();
        FontMetrics fontMetrics = g2.getFontMetrics();
        int size = font.getSize();
        String name = font.getName();
        int style = font.getStyle();

        while ( !fontFits ) {
            if ( (fontMetrics.getHeight() <= maxCharHeight)
                 && (fontMetrics.stringWidth(longString) <= xSpace) ) {
                fontFits = true;
            }
            else {
                if ( size <= minFontSize ) {
                    fontFits = true;
                } else {
                    g2.setFont(font = new Font(name, style, --size));
                    fontMetrics = g2.getFontMetrics();
                }
            }
        }

        return fontMetrics;
    }

    public void paint() {
		int si, sj;
		int i, j;
        int[] unitWidth = new int[size.length];
        for(i=0; i<size.length; i++){ //dataset
        	unitWidth[i] = calculateUnitWidth(size[i]);
        }

		for(si=0; si<numDatasetToShow; si++){

		int size_x = calculateWidth(size[si])  + 2*horizontalDatasetSpacing;
		int size_y = verticalUnitHeight;

		for(sj=0; sj<numGeneToShow; sj++){

        Graphics2D g2 = image[si][sj].createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
        	RenderingHints.VALUE_ANTIALIAS_ON);

        g2.setPaint(bg);
        g2.fill(new Rectangle2D.Double(0, 0, size_x, size_y));
        g2.setPaint(fg);

		int x = horizontalDatasetSpacing;
		int y = 0;
        for(j=0; j<size[si]; j++){ //column in dataset
        	if(matrix[si][j][sj]>327f){
        		g2.setPaint(new Color(128, 128, 128));
        	}else{
        		g2.setPaint(new Color(
        			color_red[si][j][sj], color_green[si][j][sj], 0));
        	}
        	g2.fill(new Rectangle2D.Double(x, y, unitWidth[si], 
        		verticalUnitHeight));
        	x+=unitWidth[si];
        }
        }
		}

    }
   
	public String[] getGeneNames(){
		return names;
	}
 
    /*public String getStem(String s){
    	int i = s.indexOf("_");
    	int j = s.indexOf(".");
    	if(i!=-1){
    		return s.substring(0, i);
    	}
    	return s.substring(0, j);
    }*/
    
    public Vector<String> getDatasets(){
    	return dsets;
    }
    
    public Vector<String> getGenes(){
    	return genes;
    }
    
    public byte[] getDatasetByte() throws IOException{
    	int size = 4;
    	int i, j;
    	for(i=0; i<dsets.size(); i++){
    		size += dsets.get(i).length();
    	}
    	size += dsets.size(); //for tabs and final '\0' character
    	byte[] b = new byte[size];
    	//System.out.println(Integer.toHexString(size));
    	
    	byte[] ii = intToByteArray(size-4);
    	int k = 0;
    	for(i=0; i<4; i++){
    		b[k] = ii[i];
    		//System.out.printf("%2X ", b[k]);
    		k++;
    	}
    	
    	for(i=0; i<dsets.size(); i++){
    		for(j=0; j<dsets.get(i).length(); j++){
    			b[k] = (byte) dsets.get(i).charAt(j);
    			//System.out.printf("%2X ", b[k]);
    			k++;
    		}
    		if(i==dsets.size()-1){
    			b[k] = (byte) '\0';
    			//System.out.printf("%2X ", b[k]);
    		}else{
    			b[k] = (byte) '\t';
    			//System.out.printf("%2X ", b[k]);
    		}
    		k++;
    	}
    	System.out.println(size + " " + k);
    	return b;
    }
    
    public byte[] intToByteArray(int value) {
    	ByteBuffer buf = ByteBuffer.allocate(10);
    	buf.order(ByteOrder.LITTLE_ENDIAN);
    	buf.putInt(value);
    	return new byte[] {
    		buf.get(0), buf.get(1), buf.get(2), buf.get(3) };
    }
    
    public byte[] getGeneByte() throws IOException{
    	int size = 4;
    	int i, j;
    	for(i=0; i<genes.size(); i++){
    		size += genes.get(i).length();
    	}
    	size += genes.size(); //for tabs and final '\0' character
    	byte[] b = new byte[size];
    	
    	byte[] ii = intToByteArray(size-4);
    	int k = 0;
    	for(i=0; i<4; i++){
    		b[k] = ii[i];
    		k++;
    	}
    	
    	for(i=0; i<genes.size(); i++){
    		for(j=0; j<genes.get(i).length(); j++){
    			b[k] = (byte) genes.get(i).charAt(j);
    			k++;
    		}
    		if(i==genes.size()-1){
    			b[k] = (byte) '\0';
    		}else{
    			b[k] = (byte) '\t';
    		}
    		k++;
    	}
    	return b;
    }

    public void read(String query, String gm, String dm, int numD, int numG) 
    	throws IOException{
    		
    	Scanner sc = null;
    	dsets = new Vector<String>();
    	genes = new Vector<String>();
    	this.numDatasetToShow = numD;
    	this.numGeneToShow = numG;
    	
    	try{
    		sc = new Scanner(new BufferedReader(new FileReader(query)));
    		String s1 = sc.nextLine();
    		StringTokenizer st = new StringTokenizer(s1);
    		while(st.hasMoreTokens() && dsets.size()<numD)
    			dsets.add(st.nextToken() + ".bin");
    		s1 = sc.nextLine();
    		st = new StringTokenizer(s1);
    		while(st.hasMoreTokens() && genes.size()<numG)
    			genes.add(st.nextToken());
    	}finally{
    		sc.close();
    	}
    	
    	map_genes = new HashMap<String, String>();
    	try{
    		sc = new Scanner(new BufferedReader(new FileReader(gm)));
    		while(sc.hasNext()){
    			String s1 = sc.nextLine();
    			StringTokenizer st = new StringTokenizer(s1, "\t");
    			String s2 = st.nextToken();
    			String s3 = st.nextToken();
    			map_genes.put(s2, s3);
    		}
    		//System.out.println(s2 + " " + s3);
    	}finally{
    		sc.close();
    	}
    		
    	map_datasets = new HashMap<String, String>();
    	try{
    		sc = new Scanner(new BufferedReader(new FileReader(dm)));
    		while(sc.hasNext()){
    			String s1 = sc.nextLine();
    			StringTokenizer st = new StringTokenizer(s1, "\t");
    			String s2 = st.nextToken();
    			String s3 = st.nextToken();
    			map_datasets.put(s2, s3);
    		}
    	}finally{
    		sc.close();
    	}
    	
    }
    
    public void Load(String queryFile, String geneMapFile, String datasetMapFile, 
		String imageFile, int numD, int numG) throws IOException{
    	Socket kkSocket = null;
        this.read(queryFile, geneMapFile, datasetMapFile, numD, numG);
        byte[] dByte = this.getDatasetByte();
        byte[] gByte = this.getGeneByte();
        ByteArrayInputStream bis = null;
        ObjectInputStream ois = null;
        int[] datasetSize;
        byte[] ss = null;
        float[][][] mat = null;
        ByteBuffer buf = null;
       
    	try{
    		kkSocket = new Socket("localhost", 9001);
    		kkSocket.getOutputStream().write(dByte, 0, dByte.length);
    		kkSocket.getOutputStream().write(gByte, 0, gByte.length);
    		
    		ss = new byte[8];
    		kkSocket.getInputStream().read(ss, 0, ss.length);
    		int totBytes, datasets, genes;
    		
    		buf = ByteBuffer.wrap(ss);
    		buf.order(ByteOrder.LITTLE_ENDIAN);
    	
    		totBytes = buf.getInt(); System.out.println(totBytes);
    		datasets = (int) buf.getShort();
    		genes = (int) buf.getShort();
    		
    		datasetSize = new int[datasets];
    		ss = new byte[2*datasets];
    		kkSocket.getInputStream().read(ss, 0, ss.length);
    		
    		buf = ByteBuffer.wrap(ss);
    		buf.order(ByteOrder.LITTLE_ENDIAN);
    		for(int i=0; i<datasets; i++){
    			datasetSize[i] = (int) buf.getShort();
    		}
    		
    		mat = new float[datasets][][];
    		
    		for(int i=0; i<datasets; i++){
    			mat[i] = new float[datasetSize[i]][];
    			ss = new byte[2*genes*datasetSize[i]];
    			if(ss.length>1024){
    				int nt = (int) (ss.length / 1024);
    				int ind = 0;
    				byte[] ss_tmp = new byte[1024];
    				for(int t=0; t<nt; t++){
    					kkSocket.getInputStream().read(ss_tmp, 0, ss_tmp.length);
    					for(int x=0; x<ss_tmp.length; x++){
    						ss[ind++] = ss_tmp[x];
    					}
    				}
    				if(ss.length % 1024 > 0){
    					ss_tmp = new byte[ss.length % 1024];
    					kkSocket.getInputStream().read(ss_tmp, 0, ss_tmp.length);
    					for(int x=0; x<ss_tmp.length; x++){
    						ss[ind++] = ss_tmp[x];
    					}
    				}
    						
    			}else{
    				kkSocket.getInputStream().read(ss, 0, ss.length);
    			}
    			buf = ByteBuffer.wrap(ss);
    			buf.order(ByteOrder.LITTLE_ENDIAN);
    			for(int k=0; k<datasetSize[i]; k++){
    				mat[i][k] = new float[genes];
    			}
    			
    			for(int g=0; g<genes; g++){
    				for(int k=0; k<datasetSize[i]; k++){
    					mat[i][k][g] = (float) ((int) buf.getShort() / 100.0f);
    					//System.out.printf("%.2f\n", mat[i][k][g]);
    				}
    			}
    		}
    		
    		this.init(mat);
    		this.paint();
    		this.save(imageFile);
    		
    	}catch(IOException e){
    	}
    	 //applet.init();
        //applet.paint();
        //applet.save("test.png");
    }

}
