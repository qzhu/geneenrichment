package seek;
import seek.Canvas;
import seek.TreeNode;
import seek.Cluster;
import java.io.*;
import java.net.*;
import java.nio.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.*;
import javax.swing.*;
import javax.imageio.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.cli.HelpFormatter;


public class CheckBiCluster{

	public static float getSmallest(float[][] distance){
		float small = 9999;
		for(int i=0; i<distance.length; i++){
			for(int j=0; j<distance.length; j++){
				if(small>distance[i][j]){
					small = distance[i][j];
				}
			}
		}
		return small;
	}

	public static float getLargest(float[][] distance){
		float large = -1.0f;
		for(int i=0; i<distance.length; i++){
			for(int j=0; j<distance.length; j++){
				if(large<distance[i][j]){
					large = distance[i][j];
				}
			}
		}
		return large;
	}

	public static float get_mean(float[] v){
		float sum = 0;
		for(int i=0; i<v.length; i++){
			sum+=v[i];
		}
		sum /= (float) v.length;
		return sum;
	}

	public static float get_stdev(float[] v, float mean){
		float sd = 0;
		for(int i=0; i<v.length; i++){
			sd += (v[i] - mean) * (v[i] - mean);
		}
		sd /= (float) (v.length - 1);
		sd = (float) Math.sqrt(sd);
		return sd;
	}

	public static Vector<String> readOrder(String file_order) throws IOException{
		Scanner sc = null;
		Vector<String> vs = new Vector<String>();
		try{
			//format: a list of genes (each gene per line, in input file's order)
			sc = new Scanner(new BufferedReader(new FileReader(file_order)));
			while(sc.hasNext()){
				String s1 = sc.nextLine();
				vs.add(s1);
			}
			sc.close();
		}catch(IOException e){
		}
		return vs;
	}

	public static float checkSize(int dim1, int dim2, int[][] iMatrix){
		int nG = dim1;
		int nA = dim2;
		float[][] rowDistance = new float[nG][nG];
		float[][] colDistance = new float[nA][nA];	
		float[][] fMatrix_rNorm = new float[nG][nA];
		float[][] fMatrix_cNorm = new float[nG][nA];
	
		//Preparing the matrix for calculating distance	
		for(int i=0; i<nG; i++){
			float[] v = new float[nA];
			for(int j=0; j<nA; j++){
				v[j] = (float) iMatrix[i][j];
			}
			boolean isSame = true;
			for(int j=0; j<nA; j++){
				if(v[j]!=v[0]){
					isSame = false;
					break;
				}
			}
			if(isSame){ //add a factor if all array elements are same
				v[0] += 0.1;
			}
			float mean = get_mean(v);
			float stdev = get_stdev(v, mean);
 			for(int j=0; j<nA; j++){
				fMatrix_rNorm[i][j] = (v[j] - mean) / stdev;
			}
		}

		for(int i=0; i<nA; i++){
			float[] v = new float[nG];
			for(int j=0; j<nG; j++){
				v[j] = (float) iMatrix[j][i];
			}
			boolean isSame = true;
			for(int j=0; j<nG; j++){
				if(v[j]!=v[0]){
					isSame = false;
					break;
				}
			}
			if(isSame){ //add a factor if all array elements are same
				v[0] += 0.1;
			}
			float mean = get_mean(v);
			float stdev = get_stdev(v, mean);
 			for(int j=0; j<nG; j++){
				fMatrix_cNorm[j][i] = (v[j] - mean) / stdev;
			}
		}

		//gene-gene distance
		System.out.println("calculating gene-gene distance...");
		for(int i=0; i<nG; i++){
			for(int j=i; j<nG; j++){
				float sum = 0;
				for(int k=0; k<nA; k++){
					sum += fMatrix_rNorm[i][k] * fMatrix_rNorm[j][k];
				}
				float r = 1.0f / (float) (nA - 1) * sum;
				rowDistance[i][j] = r;
				rowDistance[j][i] = r;
			}
		}

		//column-column distance
		System.out.println("calculating column-column distance...");
		for(int i=0; i<nA; i++){
			for(int j=i; j<nA; j++){
				float sum = 0;
				for(int k=0; k<nG; k++){
					sum += fMatrix_cNorm[k][i] * fMatrix_cNorm[k][j];
				}
				float r = 1.0f / (float) (nG - 1) * sum;
				colDistance[i][j] = r;
				colDistance[j][i] = r;
			}
		}
		//normalize correlation to become distances
		float smallest, largest;
		smallest = getSmallest(rowDistance);
		for(int i=0; i<rowDistance.length; i++)
			for(int j=0; j<rowDistance.length; j++)
				rowDistance[i][j] += smallest * -1.0f;
		largest = getLargest(rowDistance);
		for(int i=0; i<rowDistance.length; i++)
			for(int j=0; j<rowDistance.length; j++)
				rowDistance[i][j] /= largest;
		for(int i=0; i<rowDistance.length; i++)
			for(int j=0; j<rowDistance.length; j++)
				rowDistance[i][j] = 1.0f - rowDistance[i][j];
		float gene_smallest = smallest;
		float gene_largest = largest;
		System.out.printf("Gene-distance: smallest: %.3f, largest: %.3f\n", smallest, largest);

		//normalize correlation to become distances
		smallest = getSmallest(colDistance);
		for(int i=0; i<colDistance.length; i++)
			for(int j=0; j<colDistance.length; j++)
				colDistance[i][j] += smallest * -1.0f;
		largest = getLargest(colDistance);
		for(int i=0; i<colDistance.length; i++)
			for(int j=0; j<colDistance.length; j++)
				colDistance[i][j] /= largest;
		for(int i=0; i<colDistance.length; i++)
			for(int j=0; j<colDistance.length; j++)
				colDistance[i][j] = 1.0f - colDistance[i][j];
		System.out.printf("Column-distance: smallest: %.3f, largest: %.3f\n", smallest, largest);

		System.out.println("Begin gene clustering...");
		Cluster cr = new Cluster(rowDistance);
		TreeNode[] tr = cr.HClusterAvg();
		Vector<Vector<Integer> > vcr = cr.Bifurcate(tr);

		System.out.printf("Gene split: %d %d\n", vcr.get(0).size(), vcr.get(1).size());

		System.out.println("Begin column clustering...");
		Cluster cc = new Cluster(colDistance);
		TreeNode[] tc = cc.HClusterAvg();
		Vector<Vector<Integer> > vcc = cc.Bifurcate(tc);

		System.out.printf("Column split: %d %d\n", vcc.get(0).size(), vcc.get(1).size());

		Vector<Integer> r_order = new Vector<Integer>();
		r_order.addAll(vcr.get(0));
		r_order.addAll(vcr.get(1));
		int[] row_order = new int[rowDistance.length];
		for(int i=0; i<row_order.length; i++)
			row_order[i] = r_order.get(i);

		Vector<Integer> c_order = new Vector<Integer>();
		c_order.addAll(vcc.get(0));
		c_order.addAll(vcc.get(1));
		int[] col_order = new int[colDistance.length];
		for(int i=0; i<col_order.length; i++)
			col_order[i] = c_order.get(i);

		int[][] new_matrix = new int[nG][nA];
		for(int i=0; i<row_order.length-1; i++){
			for(int j=0; j<col_order.length-1; j++){
				new_matrix[i][j] = iMatrix[row_order[i]][col_order[j]];
			}
		}

		int row_cutpoint = vcr.get(0).size();
		int col_cutpoint = vcc.get(0).size();

		int impurities = 0;

		int ones = 0;
		int zeros = 0;
		for(int i=0; i<row_cutpoint; i++){
			for(int j=0; j<col_cutpoint; j++){
				if(new_matrix[i][j]==1) ones++;
				else zeros++;
			}
		}
		impurities += Math.min(ones, zeros);
		System.out.printf("Left-upper corner: %d %d\n", ones, zeros);

		ones = 0;
		zeros = 0;
		for(int i=row_cutpoint; i<row_order.length; i++){
			for(int j=0; j<col_cutpoint; j++){
				if(new_matrix[i][j]==1) ones++;
				else zeros++;
			}
		}
		impurities += Math.min(ones, zeros);
		System.out.printf("Right-upper corner: %d %d\n", ones, zeros);

		ones = 0;
		zeros = 0;
		for(int i=0; i<row_cutpoint; i++){
			for(int j=col_cutpoint; j<col_order.length; j++){
				if(new_matrix[i][j]==1) ones++;
				else zeros++;
			}
		}
		impurities += Math.min(ones, zeros);
		System.out.printf("Left-lower corner: %d %d\n", ones, zeros);

		ones = 0;
		zeros = 0;
		for(int i=row_cutpoint; i<row_order.length; i++){
			for(int j=col_cutpoint; j<col_order.length; j++){
				if(new_matrix[i][j]==1) ones++;
				else zeros++;
			}
		}
		impurities += Math.min(ones, zeros);
		System.out.printf("Right-lower corner: %d %d\n", ones, zeros);
	
		float impurity_percentage = (float) impurities / (nG * nA);
	
		System.out.printf("Impurity percentage: %.3f\n", impurity_percentage);
		return impurity_percentage;

	} 


	public static void main(String[] args) throws IOException{
		Options opt = new Options();
		opt.addOption("i", true, "Input file");
		opt.addOption("o", true, "Output file (clustered matrix)");
		opt.addOption("e", false, "Input file contains header (default: false)");

		opt.getOption("i").setRequired(true);
		opt.getOption("o").setRequired(true);
		opt.getOption("e").setRequired(false);

		boolean containsHeader = false;
		CommandLineParser parser = new PosixParser();
		CommandLine cmd = null;
		try{
			cmd = parser.parse(opt, args);
		}catch(Exception e){
			System.out.println("Error parsing command args!");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("CheckBiCluster", opt);
			return;
		}

		if(cmd.hasOption("e")){
			containsHeader = true;
		}
		String input_file = cmd.getOptionValue("i"); //input matrix file (0/1)
		String output_file = cmd.getOptionValue("o"); //input matrix file (0/1)
		Scanner sc = null;
		Vector<Vector<Integer> > vi = new Vector<Vector<Integer> >();
		int[][] iMatrix = null;
		Vector<String> vecHeader = new Vector<String>();
		Vector<String> vecGenes = new Vector<String>();
		int nG = 0;
		int nA = 0;
		try{
			sc = new Scanner(new BufferedReader(new FileReader(input_file)));
			//with Header
			if(containsHeader){
				String header = sc.nextLine();
				StringTokenizer st_header = new StringTokenizer(header, "\t");
				st_header.nextToken();
				while(st_header.hasMoreTokens()){
					vecHeader.add(st_header.nextToken());
				}
				while(sc.hasNext()){
					String s1 = sc.nextLine();
					StringTokenizer st = new StringTokenizer(s1, "\t");
					String g = st.nextToken();
					vecGenes.add(g);
					Vector<Integer> vs = new Vector<Integer>();
					while(st.hasMoreTokens()){
						vs.add(Integer.parseInt(st.nextToken()));
					}
					vi.add(vs);
				}
			}
			else{ //reading without header
				while(sc.hasNext()){
					String s1 = sc.nextLine();
					StringTokenizer st = new StringTokenizer(s1, "\t");
					Vector<Integer> vs = new Vector<Integer>();
					while(st.hasMoreTokens()){
						vs.add(Integer.parseInt(st.nextToken()));
					}
					vi.add(vs);
				}
			}
			nG = vi.size();
			nA = vi.get(0).size();
			iMatrix = new int[nG][nA];
			for(int i=0; i<nG; i++){
				for(int j=0; j<nA; j++){
					iMatrix[i][j] = vi.get(i).get(j);
				}
			}
		}catch(IOException e){
		}

		Map<Integer, Vector<Integer> > mm = null; 
		float tolerance = 0.10f;
		while(true){
			mm = new HashMap<Integer, Vector<Integer> >();
			int max_i = 2000;

			int max_maxj = 10;

			int gene_start = 10;
			int gene_incr = 10;

			for(int i=gene_start; i<max_i; i+=gene_incr){
				int max_j = -1;
				int hope_j = 0;
				for(int j=max_maxj; j<iMatrix[0].length; j+=10){			
				//for(int j=10; j<iMatrix[0].length; j+=10){			
					float imp = checkSize(i, j, iMatrix);
					//if(imp>0.10f){
					if(imp>tolerance){
						if(i>=1000 && imp>tolerance*2) break;
						if(j>=50 && imp>tolerance*2) break;
						hope_j = 1;
						continue;
					}
					System.out.printf("Size %d %d ==================\n", i, j);
					max_j = j;
				}
				if(max_j==-1){
					if(hope_j==0) break;
					continue;
				}
				if(max_maxj < max_j){
					max_maxj = max_j;
				}
				if(mm.get(max_j)==null){
					mm.put(max_j, new Vector<Integer>());
				}
				Vector<Integer> vt = mm.get(max_j);
				vt.add(i);
				mm.put(max_j, vt);

				if(i==max_i-gene_incr){
					//test if we need to add more i
					Set<Integer> keys_mm = mm.keySet();
					int key_max = Collections.max(keys_mm);
					int key_value_max = Collections.max(mm.get(key_max));
					if(key_value_max==max_i-gene_incr){
						max_i += 1000;
					}
				}

			}
			if(mm.size()==0){
				if(tolerance>0.15f){
					System.out.println("WARNING: No more bi-clusters!!!");
					break;
				}
				tolerance+=0.01f;
			}else{
				break;
			}
		}

		Set<Integer> keys_mm = mm.keySet();
		int key_max = Collections.max(keys_mm);
		int key_value_max = Collections.max(mm.get(key_max));
		System.out.printf("Size Final %d %d ===============\n", key_max, key_value_max);

	}
}
