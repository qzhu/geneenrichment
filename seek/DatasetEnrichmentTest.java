package seek;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.math3.distribution.HypergeometricDistribution;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.cli.HelpFormatter;
import java.io.*;
import java.util.*;
import seek.GeneSet;
import seek.Pair;

public class DatasetEnrichmentTest {

	public static boolean doCommand(String cmd) throws Exception{
		try{
			Process p = Runtime.getRuntime().exec(cmd);
			BufferedReader in=new BufferedReader(new InputStreamReader(p.getInputStream())); 
			BufferedReader err=new BufferedReader(new InputStreamReader(p.getErrorStream())); 
			String s = null;
			while ((s=in.readLine())!=null){
			}
			while ((s=err.readLine())!=null){
			}
			in.close();
			err.close();
		}catch(Exception e){
			System.out.println("Error");
			return false;
		}
		return true;
	}

	public static boolean doCommand(String[] cmd) throws Exception{
		try{
			Process p = Runtime.getRuntime().exec(cmd);
			BufferedReader in=new BufferedReader(new InputStreamReader(p.getInputStream())); 
			BufferedReader err=new BufferedReader(new InputStreamReader(p.getErrorStream())); 
			String s = null;
			while ((s=in.readLine())!=null){
			}
			while ((s=err.readLine())!=null){
			}
			in.close();
			err.close();
		}catch(Exception e){
			System.out.println("Error");
			return false;
		}
		return true;
	}
	public static Vector<String> ReadRanks(String filename) throws IOException{
		Vector<String> ranks = new Vector<String>();
		try{
			BufferedReader in = new BufferedReader(new FileReader(filename));
			String s = null;
			int i = 0;
			while((s=in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(s, "\t");
				st.nextToken();
				ranks.add(st.nextToken());
				st.nextToken();
			}
		}catch(IOException e){
			System.out.println("Error opening file " + filename);
		}
		return ranks;
	}

	//GSEXXX.GPLXXX (one line (space separated), or multi-line)
	public static Vector<Pair> ReadDatasetList(String filename) throws IOException{
		Vector<Pair> vp = new Vector<Pair>();
		try{
			BufferedReader in = new BufferedReader(new FileReader(filename));
			String s = null;
			int i = 0;
			Vector<String> ss = new Vector<String>();
			while((s=in.readLine())!=null){
				ss.add(s);
			}
			StringTokenizer st = new StringTokenizer(ss.get(0), " ");
			boolean oneLine = false;
			if(st.countTokens()>=3){
				oneLine = true;
			}
			if(oneLine){
				i = 0;
				while(st.hasMoreTokens()){
					vp.add(new Pair(st.nextToken(), (float) i, -1));
					i++;
				}
			}else{
				i = 0;
				for(String xs : ss){
					Pair np = new Pair(xs, (float) i, -1);
					vp.add(np);
					i++;
				}
			}
		}catch(IOException e){
			System.out.println("Error opening file " + filename);
		}
		return vp;
	}

	public static void main(String[] args) throws IOException{
		Options opt = new Options();
		opt.addOption("m", true, "Input mode and file. <0> <sessionID> (only if Tomcat6 " + 
			"environment), <1> <dweight file>, <2> <gse_id.platform_id list in text>");
		opt.addOption("l", true, "Setting directory. Specify where dataset/" +
			"platform mapping, dataset annotations are stored. (Default: Tomcat work directory)");
		opt.addOption("t", true, "Top X datasets to analyze.");
		opt.addOption("s", true, "Annotations to use. (default)");
		opt.addOption("g", false, "Show overlapped datasets.");
		opt.getOption("m").setArgs(3);
		opt.getOption("m").setRequired(true);
		opt.getOption("l").setRequired(false);
		opt.getOption("t").setRequired(true);
		opt.getOption("s").setRequired(true);

		CommandLineParser parser = new PosixParser();
		CommandLine cmd = null;
		try{
			cmd = parser.parse(opt, args);
		}catch(Exception e){
			System.out.println("Error parsing command args!");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("DatasetEnrichmentTest", opt);
			return;
		}

		String tempDir = "/home/qzhu/hierarchical/enrichment";
		if(cmd.hasOption("l")){
			tempDir = new String(cmd.getOptionValue("l"));
		}
		boolean showDatasets = false;
		if(cmd.hasOption("g")){
			showDatasets = true;
		}

		int mode = Integer.parseInt(cmd.getOptionValues("m")[0]);
		File tempFile = null;
		if(mode==0){
			tempFile = new File(tempDir + "/" + cmd.getOptionValues("m")[1] + "_dweight");
		}else if(mode==1){
			tempFile = new File(cmd.getOptionValues("m")[1]);
		}else if(mode==2){
			tempFile = new File(cmd.getOptionValues("m")[1]);
		}else{
			System.out.println("Bad");
			return;
		}

		int top = Integer.parseInt(cmd.getOptionValue("t")); //all annotated genes in top X
		String goldstd = cmd.getOptionValue("s");
	
		//read dataset platform mapping
		//String s1 = tempDir + "/" + "gene_entrez_symbol.txt";
		String s1 = tempDir + "/" + "dataset_platform.jul7";
		Map<String, Integer> dset_int = new HashMap<String,Integer>();
		Map<Integer, String> int_dset = new HashMap<Integer,String>();
		Map<String, Vector<String> > gse_index = new HashMap<String, Vector<String> >();

		ReadScore.readDatasetPlatformMap(s1, dset_int, int_dset, gse_index);

		//Map<String, Vector<String> > mge = GeneSet.readGeneEntrezMapping(s1);
		//Map<String, Vector<String> > meg = GeneSet.convertGeneEntrez(mge);
		//Set<String> all_entrez = meg.keySet();
		//Map<Integer, String> mreverse = new HashMap<Integer, String>();
		//Map<String, Integer> mentrez = GeneSet.convertGeneNameToInteger(all_entrez);
		//System.out.println("Mapping:");
		//for(String s : mentrez.keySet()){
		//	mreverse.put(mentrez.get(s), s);
			//System.out.println(s + "\t" + mentrez.get(s));
		//}

		//read gold standard gene set
		String s2 = "";
		if(goldstd.equals("default")){
			s2 = tempDir + "/" + "default.nov4";
		}

		//String[] genes = (String[]) vecGenes.toArray();

		//String[] genes = {"SMO", "CCDC8", "MYOZ3", "PCDH18", "HEYL", "SEMA6A", "EDA", 
		//				"CRMP1", "PTCH2", "PCYT1B", "SDK2", "SYDE1", "PKDCC", "ARVCF",
		//				"IHH", "DTX3", "DCHS1", "HHIP", "NFATC4", "CPZ", "HHIP-AS1"};

		Map<String, Vector<String> > gold_std = GeneSet.readGeneSet(s2); //although GeneSet, also works for dataset annotation reading
		Map<String, Vector<Integer> > gold_std_int = GeneSet.convertGeneSetToInteger(gold_std, dset_int);

		int population = 5000; //temporary

		Set<Integer> all_datasets = new HashSet<Integer>();
		Set<String> all_dataset_names = new HashSet<String>();

		for(String term : gold_std_int.keySet()){
			for(Integer gs : gold_std_int.get(term))
				all_datasets.add(gs);
			for(String gg : gold_std.get(term))
				all_dataset_names.add(gg);
		}
		population = all_datasets.size();

		//reading gene names sorted by gene scores
		//File tempFile = new File(tempDir + "/" + sessionID);
		//String name_mapping = tempDir + "/gene_id.map";
		//Map<String, Integer> mm = ReadScore.readGeneMapping(name_mapping);		
		//System.out.println(tempFile.getAbsolutePath());
		Vector<Pair> valid = null;
		if(mode==0 || mode==1){
			float[] sc = ReadScore.ReadScoreBinary(tempFile.getAbsolutePath());
			valid = ReadScore.SortScores(dset_int, sc);
		}else if(mode==2){
			valid = ReadDatasetList(tempFile.getAbsolutePath());
		}

		//System.out.println(sc.length + " " + valid.size());
		//get top X genes (where X is a parameter from command line)
		Vector<Integer> query = new Vector<Integer>();
		if(top>valid.size()){
			System.out.println("Top exceed dataset list size, replace with max size.");
			top = valid.size();
		}

		for(int i=0; i<top; i++){
			if(all_dataset_names.contains(valid.get(i).term)){
				if(dset_int.containsKey(valid.get(i).term)) 
					query.add(dset_int.get(valid.get(i).term));
				//System.out.println(valid.get(i).term + "\t" + mentrez.get(valid.get(i).term));
			}
		}
		Collections.sort(query);
		System.out.printf("Pop: %d/%d Q: %d/%d\n", population, valid.size(), 
			query.size(), top);
		
		Vector<Pair> vp = new Vector<Pair>();
		Map<String, Vector<Integer> > map_overlap = new HashMap<String, Vector<Integer> >();
		for(String term : gold_std_int.keySet()){
			Vector<Integer> overlap = GeneSet.getGeneSetOverlapGenes(query, gold_std_int.get(term));
			//System.out.println(term);
			//if(overlap.size()<=1) continue;
			map_overlap.put(term, new Vector<Integer>(overlap));
			int successes = gold_std_int.get(term).size();
			HypergeometricDistribution hyp = new HypergeometricDistribution(population, successes, query.size());
			double prob = hyp.upperCumulativeProbability(overlap.size());
			if(prob>=1.0){
				prob = 0.999999999;
			}	
			Pair pp = new Pair(term, prob, -1);
			vp.add(pp);
		}	
		Collections.sort(vp);

		/*for(Pair p : vp){
			System.out.println(p.term + "\t" + p.val);
		}*/		
		//String st = tempDir + "/" + sessionID + ".pval";
		String st = "/tmp/" + tempFile.getName() + ".pval";
		PrintWriter out1 = null;
		try{
			out1 = new PrintWriter(new FileWriter(st));
			for(int i=0; i<vp.size(); i++){
				Pair pi = vp.get(i);
				out1.println(pi.val);
				//System.out.println(pi.val);
			}
			//System.out.println("Done");
		}catch(Exception e){
			System.out.println("BAD IO 1");
			//e.printStackTrace();
			return ;
		}finally{
			if(out1 !=null) out1.close();
		}
		
		//String sv = tempDir + "/" + sessionID + ".qval";
		String sv = "/tmp/" + tempFile.getName() + ".qval";
		try{
			if(!doCommand(tempDir + "/qval.R " + st + " " + sv)){
				System.out.println("Error!");
				return ;
			}
		}catch(Exception e){
			System.out.println("exception happened - here's what I know: ");
			e.printStackTrace();
		}

		Vector<Float> qval = new Vector<Float>();
		BufferedReader in = null;
		try{
			in = new BufferedReader(new FileReader(sv));
			String s = null;
			while((s=in.readLine())!=null){
				qval.add(Float.parseFloat(s));
			}
		}catch(IOException e){
			System.out.println("BAD IO 2");
			return ;
		}finally{
			if(in!=null) in.close();
		}

		float prev_value = 0;
		for(int i=0; i<vp.size(); i++){
			Pair pi = vp.get(i);
			float b_value = (float) pi.val * (float) gold_std_int.get(pi.term).size() / (float) (i+1);
			if(b_value>1f){
				b_value = 1.0f;
			}
			if(b_value<prev_value){
				b_value = prev_value;
			}
			prev_value = b_value;

			if(goldstd.equals("targetscan") || goldstd.equals("targetscan_family")){
				if(map_overlap.get(pi.term).size()<=1) continue;
				if(pi.val>=0.05 || qval.get(i)>0.5) continue;
				System.out.printf("%s\t%.1E\t%.1E\t%d\t%d\n", pi.term, b_value, qval.get(i),
					gold_std_int.get(pi.term).size(), map_overlap.get(pi.term).size());

				//System.out.println(pi.term + "\t" + b_value + "\t" + qval.get(i) +"\t" + 
				//	gold_std_int.get(pi.term).size() + "\t" + 
					//query.size() + "\t" + 
				//	map_overlap.get(pi.term).size());
				/*for(Integer ii : map_overlap.get(pi.term))
					for(String ss : meg.get(mreverse.get(ii)))
						System.out.print(ss + " ");		
				System.out.println();
				*/
			}else{
				//if(!geneSets.contains(pi.term)){
				//	continue;
				//}
				if(pi.val>=0.05 || qval.get(i)>0.25){
					break;
				}
				System.out.printf("%s\t%.1E\t%.1E\t%d\t%d\n", pi.term, b_value, qval.get(i),
					gold_std_int.get(pi.term).size(), map_overlap.get(pi.term).size());
				//System.out.println(pi.term + "\t" + b_value + "\t" + qval.get(i) + "\t" 
				//	+ gold_std_int.get(pi.term).size() + "\t" +
					//query.size() + "\t" + 
				//	+ map_overlap.get(pi.term).size());
			}
			if(showDatasets){
				for(Integer ii : map_overlap.get(pi.term))
					//print entrez gene id
					System.out.print(int_dset.get(ii) + " ");
				System.out.println();
			}

		}
	}
}
