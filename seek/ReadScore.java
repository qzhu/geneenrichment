package seek;
import java.nio.*;
import org.apache.commons.lang3.StringUtils;
import java.io.*;
import java.util.*;
import seek.Pair;

public class ReadScore{

	//entrez to gene(internal ID) mapping
	public static Map<String, Integer> readGeneMapping(String fileEntrez2ID)
		throws IOException{

		//read entrez to ID mapping
		Map<String, Integer> ent_id = new HashMap<String, Integer>();
		try{
			BufferedReader in = new BufferedReader(new FileReader(fileEntrez2ID));
			String s = null;
			while((s = in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(s, "\t");
				String id = st.nextToken();
				String ent = st.nextToken();
				ent_id.put(ent, Integer.parseInt(id));
			}
		}catch(IOException e){
			System.out.println("Error opening file 1");

		}
		return ent_id;
	}

	public static Map<String, String> readGeneSymbol2EntrezMapping(
		String file){
		Map<String, String> symbol_ent = new HashMap<String, String>();
		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			String s = null;
			while((s = in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(s, "\t");
				String entrez = st.nextToken();
				String gene_name = st.nextToken();
				symbol_ent.put(gene_name, entrez);
			}
			in.close();
		}catch(IOException e){	
			System.out.println("Error opening file Gene symbol 2 entrez");
		}
		return symbol_ent;
	}

	//returns mapping two ways
	public static void readGeneEntrezSymbolMap(String fileEntrez2HGNC,
		Map<String, String> ent_hgnc, Map<String, String> hgnc_ent) throws IOException{
		try{
			BufferedReader in = new BufferedReader(new FileReader(fileEntrez2HGNC));
			String s = null;
			while((s = in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(s, "\t");
				String entrez = st.nextToken();
				String gene_name = st.nextToken();
				ent_hgnc.put(entrez, gene_name);
				hgnc_ent.put(gene_name, entrez);
			}
			in.close();
		}catch(IOException e){	
			System.out.println("Error opening file 0");
		}
	}

	//read dataset mapping three ways
	public static void readDatasetPlatformMap(String file, 
		Map<String,Integer> dset_int, Map<Integer,String> int_dset, 
		Map<String,Vector<String> > gse_index) throws IOException {
		
		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			String s = null;
			int i = 0;
			String gse_id = "";
			while((s=in.readLine())!=null){
				//making dset_int, and int_dset
				StringTokenizer st = new StringTokenizer(s, "\t");
				String dataset = st.nextToken();
				String[] dd = StringUtils.split(dataset, ".");
				String dset = dd[0] + "." + dd[1]; //no pcl ending 
				dset_int.put(dset, i);
				int_dset.put(i, dset);
				i++;

				//making gse_index
				int gid = 0;
				if(s.contains("_")){
					gid = s.indexOf("_");
				}else{
					gid = s.indexOf(".");
				}
				gse_id = s.substring(0, gid);

				Vector<String> vs;
				if(gse_index.containsKey(gse_id)){
					vs = gse_index.get(gse_id);
				}else{
					vs = new Vector<String>();
				}
				vs.add(dset);
				gse_index.put(gse_id, vs);

			}
			in.close();
		}catch(IOException e){
			System.out.println("Error opening file x");
		}
	}

	//read entrez to ID mapping 2 ways
	public static void readEntrezIntMap(String file, Map<String,Integer> ent_int,
		Map<Integer, String> int_ent) throws IOException{
		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			String s = null;
			while((s = in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(s, "\t");
				String id = st.nextToken();
				String ent = st.nextToken();
				int ii = Integer.parseInt(id);
				ent_int.put(ent, ii);
				int_ent.put(ii, ent);
			}
		}catch(IOException e){
			System.out.println("Error opening file 1");
		}
	}

	public static Map<String, String> readGeneEntrez2Sym(String fileEntrez2HGNC) throws
		IOException{
		Map<String, String> ent_hgnc = new HashMap<String, String>();
		try{
			BufferedReader in = new BufferedReader(new FileReader(fileEntrez2HGNC));
			String s = null;
			while((s = in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(s, "\t");
				String entrez = st.nextToken();
				String gene_name = st.nextToken();
				ent_hgnc.put(entrez, gene_name);
			}
			in.close();
		}catch(IOException e){	
			System.out.println("Error opening file 0");
		}
		return ent_hgnc;
	}

	//return hgnc to ID mapping
	public static Map<String, Integer> readGeneMapping(String fileEntrez2ID, 
		String fileEntrez2HGNC) throws IOException {
			
		Map<String, Integer> m = new HashMap<String, Integer>();
		
		//read entrez-HGNC mapping
		Map<String, String> hgnc_ent = new HashMap<String, String>();
		try{
			BufferedReader in = new BufferedReader(new FileReader(fileEntrez2HGNC));
			String s = null;
			while((s = in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(s, "\t");
				String entrez = st.nextToken();
				String gene_name = st.nextToken();
				hgnc_ent.put(gene_name, entrez);
			}
			in.close();
		}catch(IOException e){	
			System.out.println("Error opening file 0");

		}
		
		//read entrez to ID mapping
		Map<String, String> ent_id = new HashMap<String, String>();
		try{
			BufferedReader in = new BufferedReader(new FileReader(fileEntrez2ID));
			String s = null;
			while((s = in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(s, "\t");
				String id = st.nextToken();
				String ent = st.nextToken();
				ent_id.put(ent, id);
			}
		}catch(IOException e){
			System.out.println("Error opening file 1");

		}
		
		for(String key : hgnc_ent.keySet()){
			String val = hgnc_ent.get(key);
			if(ent_id.containsKey(val)){
				m.put(key, Integer.parseInt(ent_id.get(val)));
			}
		}
		return m;
	}

	public static Vector<Vector<String> > readQueryParts(String file)
		throws IOException {
		Vector<Vector<String> > m = new Vector<Vector<String> >();
		
		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			String s = null;
			int i = 0;
			while((s = in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(s, " ");
				Vector<String> vs = new Vector<String>();
				while(st.hasMoreTokens()){
					vs.add(st.nextToken());
				}
				m.add(vs);
				i++;
			}
			in.close();
		}catch(IOException e){	
			System.out.println("Error opening file 2");

		}	
		return m;
	}
	
	public static Map<String, Integer> readDatasetMapping(String file) 
		throws IOException {
			
		Map<String, Integer> m = new HashMap<String, Integer>();
		
		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			String s = null;
			int i = 0;
			while((s = in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(s, "\t");
				String dataset = st.nextToken();
				String[] dd = StringUtils.split(dataset, ".");
				String dset = dd[0] + "." + dd[1]; //platform and dataset name
				m.put(dset, i);
				i++;
			}
			in.close();
		}catch(IOException e){	
			System.out.println("Error opening file 2");

		}
		
		return m;
	}

	public static float[] ReadScoreBinary(String tempFile) throws IOException{
		float[] sc = null;
	
		try{
			DataInputStream in = new DataInputStream(new BufferedInputStream(
				new FileInputStream(tempFile)));
			byte[] array_size = new byte[8];
			in.read(array_size, 0, 8);
			ByteBuffer buf = null;
			buf = ByteBuffer.wrap(array_size);
			buf.order(ByteOrder.LITTLE_ENDIAN);
			long size = buf.getInt() & 0xffffffffL;
			//System.out.println("Size is " + size);
			
			byte[] ss_tmp = new byte[4*(int) size];
			in.read(ss_tmp, 0, ss_tmp.length);
			buf = ByteBuffer.wrap(ss_tmp);
			buf.order(ByteOrder.LITTLE_ENDIAN);
			sc = new float[(int) size];
			for(int i=0; i<sc.length; i++){
				sc[i] = buf.getFloat();
				//System.out.println(String.format("%.5e", sc[i]));
			}
		
			in.close();
		}catch(IOException e){
			System.out.println("Error opening file 3");
		}

		return sc;
	}

	//mm is entity mapping (one of ReadDatasetMapping or ReadGeneMapping)
	public static Vector<Pair> SortScores(Map<String, Integer> mm, float[] sc){

		Vector<String> mp = new Vector<String>(mm.keySet());
		Vector<Pair> vp = new Vector<Pair>();
		for(int i=0; i<mp.size(); i++){
			Pair np = new Pair(mp.get(i), sc[mm.get(mp.get(i))], mm.get(mp.get(i)));
			//System.out.printf("%.5f\n", sc[mm.get(mp.get(i))]);
			vp.add(np);
		}

		Collections.sort(vp);
		Collections.reverse(vp);
		Vector<Pair> valid = new Vector<Pair>();
		for(int i=0; i<vp.size(); i++){
			//System.out.printf("%.5f\n", vp.get(i).val);
			if(vp.get(i).val<-300){
				continue;
				//break;
			}
			valid.add(new Pair(vp.get(i).term, vp.get(i).val, vp.get(i).index));
		}
		return valid;
	}
	
}
