package seek;
import seek.Canvas;
import seek.TreeNode;
import seek.Cluster;
import java.io.*;
import java.net.*;
import java.nio.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.*;
import javax.swing.*;
import javax.imageio.*;


public class HierarchicalClustering{

	public static float getSmallest(float[][] distance){
		float small = 9999;
		for(int i=0; i<distance.length; i++){
			for(int j=0; j<distance.length; j++){
				if(small>distance[i][j]){
					small = distance[i][j];
				}
			}
		}
		return small;
	}

	public static float getLargest(float[][] distance){
		float large = -1.0f;
		for(int i=0; i<distance.length; i++){
			for(int j=0; j<distance.length; j++){
				if(large<distance[i][j]){
					large = distance[i][j];
				}
			}
		}
		return large;
	}

	public static void main(String[] args) throws IOException{
		String input_file = args[0];
		String dendrogram_file = args[1];

		BufferedReader rb = new BufferedReader(new FileReader(input_file));
		String s = rb.readLine();
		StringTokenizer st = new StringTokenizer(s, "\t");
		st.nextToken();
		Vector<String> genes = new Vector<String>();
		while(st.hasMoreTokens()){
			genes.add(st.nextToken());
		}
		int numGenes = genes.size();
		float[][] distance = new float[numGenes][numGenes];
		int i, j;
		i = 0;
		while((s=rb.readLine())!=null){
			st = new StringTokenizer(s, "\t");
			st.nextToken();
			j = 0;
			while(st.hasMoreTokens()){
				distance[i][j] = Float.parseFloat(st.nextToken());
				j++;
			}
			i++;
		}
		System.out.println("Done!");

		float smallest = getSmallest(distance);
		System.out.println("Added -1.0 * " + smallest);
		for(i=0; i<distance.length; i++){
			for(j=0; j<distance.length; j++){
				distance[i][j] += smallest * -1.0f;
			}
		}

		float largest = getLargest(distance);
		System.out.println("Divided by " + largest);
		for(i=0; i<distance.length; i++){
			for(j=0; j<distance.length; j++){
				distance[i][j] /= largest;
			}
		}
		
		System.out.println("1.0 - value");
		for(i=0; i<distance.length; i++){
			for(j=0; j<distance.length; j++){
				distance[i][j] = 1.0f - distance[i][j];
			}
		}

		Cluster c = new Cluster(distance);
		TreeNode[] t = c.HClusterAvg();
		System.out.println("Done Clustering");

		TreeNode[] dendro_tree = t;
		Vector<Integer> vi = c.Linearize(t);
		Map<Integer,Float> mm = c.GetDepth(t);
		Vector<Float> dist = new Vector<Float>();
		for(TreeNode tr : t){
			dist.add(tr.distance);
		}
		System.out.println("Done Processing");

		int[] sample_order = new int[numGenes];
		for(i=0; i<numGenes; i++)
			sample_order[i] = vi.get(i);

		float dendro_depth = Collections.max(dist);
		float factor = 100.0f / dendro_depth;

		System.out.println(dendro_depth);

		int dendro_height = (int) (factor * dendro_depth + 2);

		Canvas ca = new Canvas();
		ca.horizontalUnitWidth = 2;
		ca.windowWidth = 5 + ca.horizontalUnitWidth*numGenes + 10;
	
		System.out.println("Canvas dimension " + dendro_height + " " + 
			ca.windowWidth);
	
		int dsetWidth = ca.horizontalUnitWidth*numGenes;

		BufferedImage dendro_image = 
			new BufferedImage(ca.windowWidth, dendro_height,
			BufferedImage.TYPE_INT_RGB);

		Graphics2D g4 = dendro_image.createGraphics();
		g4.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
			RenderingHints.VALUE_ANTIALIAS_ON);

		Color dendro_color = Color.black;
		g4.setPaint(Color.white);
		g4.fill(new Rectangle2D.Double(0,0,ca.windowWidth,dendro_height));
		g4.setPaint(dendro_color);
		int x = 5 + ca.horizontalUnitWidth / 2;
		int y = 12;
		g4.setColor(dendro_color);
		int maxWidth = dsetWidth;
		float maxDepth = dendro_depth;
		int[] reverse = new int[numGenes];
		for(i=0; i<numGenes; i++)
			reverse[sample_order[i]] = i;
		int orig_x = x;
		Map<Integer,Float> coord_x = new HashMap<Integer,Float>();
		Map<Integer,Float> coord_y = new HashMap<Integer,Float>();
		Map<Integer,Integer> map_m = new HashMap<Integer,Integer>();
		for(i=0; i<dendro_tree.length; i++){
			map_m.put(dendro_tree[i].left, i);
			map_m.put(dendro_tree[i].right, i);
		}

		for(i=0; i<numGenes; i++){
			int a = sample_order[i];
			TreeNode tr = dendro_tree[map_m.get(a)];
			coord_x.put(a, (float) x);
			coord_y.put(a, (float) dendro_height);
			x+=ca.horizontalUnitWidth;
		}

		for(i=0; i<numGenes-1; i++){
			TreeNode tr = dendro_tree[i];
			int left = tr.left;
			int right = tr.right;
			float loc_x = (coord_x.get(left) + coord_x.get(right)) / 2.0f;
			float loc_y_left = coord_y.get(left);
			float loc_y_right = coord_y.get(right);
			g4.drawLine((int) coord_x.get(left).floatValue(), 
				(int) loc_y_left, (int) coord_x.get(left).floatValue(),
				(int) (dendro_height - tr.distance * factor));
			g4.drawLine((int) coord_x.get(right).floatValue(), 
				(int) loc_y_right, (int) coord_x.get(right).floatValue(),
				(int) (dendro_height - tr.distance * factor));

			float loc_y = dendro_height - tr.distance * factor;
			g4.drawLine((int) (coord_x.get(left).floatValue()), 
				(int) loc_y, (int) coord_x.get(right).floatValue(), (int) loc_y);
			coord_x.put((i+1)*-1, loc_x);
			coord_y.put((i+1)*-1, (dendro_height - tr.distance * factor));
		}
	
		File f = new File(dendrogram_file);
		ImageIO.write(dendro_image, "png", f);

	}
}
