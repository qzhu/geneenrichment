package seek;
import seek.Canvas;
import seek.TreeNode;
import seek.Cluster;
import java.io.*;
import java.net.*;
import java.nio.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.*;
import javax.swing.*;
import javax.imageio.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.cli.HelpFormatter;


public class HCluster{

	public static float getSmallest(float[][] distance){
		float small = 9999;
		for(int i=0; i<distance.length; i++){
			for(int j=0; j<distance.length; j++){
				if(small>distance[i][j]){
					small = distance[i][j];
				}
			}
		}
		return small;
	}

	public static float getLargest(float[][] distance){
		float large = -1.0f;
		for(int i=0; i<distance.length; i++){
			for(int j=0; j<distance.length; j++){
				if(large<distance[i][j]){
					large = distance[i][j];
				}
			}
		}
		return large;
	}

	public static float get_mean(float[] v){
		float sum = 0;
		for(int i=0; i<v.length; i++){
			sum+=v[i];
		}
		sum /= (float) v.length;
		return sum;
	}

	public static float get_stdev(float[] v, float mean){
		float sd = 0;
		for(int i=0; i<v.length; i++){
			sd += (v[i] - mean) * (v[i] - mean);
		}
		sd /= (float) (v.length - 1);
		sd = (float) Math.sqrt(sd);
		return sd;
	}

	public static Vector<String> readOrder(String file_order) throws IOException{
		Scanner sc = null;
		Vector<String> vs = new Vector<String>();
		try{
			//format: a list of genes (each gene per line, in input file's order)
			sc = new Scanner(new BufferedReader(new FileReader(file_order)));
			while(sc.hasNext()){
				String s1 = sc.nextLine();
				vs.add(s1);
			}
			sc.close();
		}catch(IOException e){
		}
		return vs;
	}

	public static void main(String[] args) throws IOException{

		Options opt = new Options();
		opt.addOption("i", true, "Input file");
		opt.addOption("o", true, "Output file (clustered matrix)");
		opt.addOption("e", false, "Input file contains header (default: false)");
		opt.addOption("k", true, "Get connected components only (need a parameter defining distance)");
		opt.addOption("m", false, "Print sorted order only, separated by left branch and right branch");

		opt.getOption("i").setRequired(true);
		opt.getOption("o").setRequired(true);
		opt.getOption("e").setRequired(false);
		opt.getOption("k").setRequired(false);
		opt.getOption("m").setRequired(false);

		boolean containsHeader = false;
		CommandLineParser parser = new PosixParser();
		CommandLine cmd = null;
		try{
			cmd = parser.parse(opt, args);
		}catch(Exception e){
			System.out.println("Error parsing command args!");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("HCluster", opt);
			return;
		}

		if(cmd.hasOption("e")){
			containsHeader = true;
		}

		boolean connectedComp = false;
		if(cmd.hasOption("k")){
			connectedComp = true;
		}

		boolean printOrder = false;
		if(cmd.hasOption("m")){
			printOrder = true;
		}

		String input_file = cmd.getOptionValue("i"); //input matrix file (0/1)
		String output_file = cmd.getOptionValue("o"); //input matrix file (0/1)

		Scanner sc = null;
		Vector<Vector<Integer> > vi = new Vector<Vector<Integer> >();
		int[][] iMatrix = null;
		Vector<String> vecHeader = new Vector<String>();
		Vector<String> vecGenes = new Vector<String>();

		int nG = 0;
		int nA = 0;

		try{
			sc = new Scanner(new BufferedReader(new FileReader(input_file)));
			//with Header
			if(containsHeader){
				String header = sc.nextLine();
				StringTokenizer st_header = new StringTokenizer(header, "\t");
				st_header.nextToken();
				while(st_header.hasMoreTokens()){
					vecHeader.add(st_header.nextToken());
				}
				while(sc.hasNext()){
					String s1 = sc.nextLine();
					StringTokenizer st = new StringTokenizer(s1, "\t");
					String g = st.nextToken();
					vecGenes.add(g);
					Vector<Integer> vs = new Vector<Integer>();
					while(st.hasMoreTokens()){
						vs.add(Integer.parseInt(st.nextToken()));
					}
					vi.add(vs);
				}
			}
			else{ //reading without header
				while(sc.hasNext()){
					String s1 = sc.nextLine();
					StringTokenizer st = new StringTokenizer(s1, "\t");
					Vector<Integer> vs = new Vector<Integer>();
					while(st.hasMoreTokens()){
						vs.add(Integer.parseInt(st.nextToken()));
					}
					vi.add(vs);
				}
			}

			nG = vi.size();
			nA = vi.get(0).size();

			iMatrix = new int[nG][nA];
			for(int i=0; i<nG; i++){
				for(int j=0; j<nA; j++){
					iMatrix[i][j] = vi.get(i).get(j);
				}
			}

		}catch(IOException e){
		}

		float[][] rowDistance = new float[nG][nG];
		float[][] colDistance = new float[nA][nA];
			
		float[][] fMatrix_rNorm = new float[nG][nA];
		float[][] fMatrix_cNorm = new float[nG][nA];
	
		//Preparing the matrix for calculating distance	
		for(int i=0; i<nG; i++){
			float[] v = new float[nA];
			for(int j=0; j<nA; j++){
				v[j] = (float) iMatrix[i][j];
			}

			boolean isSame = true;
			for(int j=0; j<nA; j++){
				if(v[j]!=v[0]){
					isSame = false;
					break;
				}
			}
			if(isSame){ //add a factor if all array elements are same
				v[0] += 0.1;
			}
					
			float mean = get_mean(v);
			float stdev = get_stdev(v, mean);
 			for(int j=0; j<nA; j++){
				fMatrix_rNorm[i][j] = (v[j] - mean) / stdev;
			}
		}

		for(int i=0; i<nA; i++){
			float[] v = new float[nG];
			for(int j=0; j<nG; j++){
				v[j] = (float) iMatrix[j][i];
			}

			boolean isSame = true;
			for(int j=0; j<nG; j++){
				if(v[j]!=v[0]){
					isSame = false;
					break;
				}
			}
			if(isSame){ //add a factor if all array elements are same
				v[0] += 0.1;
			}

			float mean = get_mean(v);
			float stdev = get_stdev(v, mean);
 			for(int j=0; j<nG; j++){
				fMatrix_cNorm[j][i] = (v[j] - mean) / stdev;
			}
		}

		//gene-gene distance
		System.out.println("calculating gene-gene distance...");
		for(int i=0; i<nG; i++){
			for(int j=i; j<nG; j++){
				float sum = 0;
				for(int k=0; k<nA; k++){
					sum += fMatrix_rNorm[i][k] * fMatrix_rNorm[j][k];
				}
				float r = 1.0f / (float) (nA - 1) * sum;
				rowDistance[i][j] = r;
				rowDistance[j][i] = r;
			}
		}

		//column-column distance
		System.out.println("calculating column-column distance...");
		for(int i=0; i<nA; i++){
			for(int j=i; j<nA; j++){
				float sum = 0;
				for(int k=0; k<nG; k++){
					sum += fMatrix_cNorm[k][i] * fMatrix_cNorm[k][j];
				}
				float r = 1.0f / (float) (nG - 1) * sum;
				colDistance[i][j] = r;
				colDistance[j][i] = r;
			}
		}

		//normalize correlation to become distances
		float smallest, largest;
		smallest = getSmallest(rowDistance);
		for(int i=0; i<rowDistance.length; i++)
			for(int j=0; j<rowDistance.length; j++)
				rowDistance[i][j] += smallest * -1.0f;
		largest = getLargest(rowDistance);
		for(int i=0; i<rowDistance.length; i++)
			for(int j=0; j<rowDistance.length; j++)
				rowDistance[i][j] /= largest;
		for(int i=0; i<rowDistance.length; i++)
			for(int j=0; j<rowDistance.length; j++)
				rowDistance[i][j] = 1.0f - rowDistance[i][j];
		float gene_smallest = smallest;
		float gene_largest = largest;
		System.out.printf("Gene-distance: smallest: %.3f, largest: %.3f\n", smallest, largest);

		//normalize correlation to become distances
		smallest = getSmallest(colDistance);
		for(int i=0; i<colDistance.length; i++)
			for(int j=0; j<colDistance.length; j++)
				colDistance[i][j] += smallest * -1.0f;
		largest = getLargest(colDistance);
		for(int i=0; i<colDistance.length; i++)
			for(int j=0; j<colDistance.length; j++)
				colDistance[i][j] /= largest;
		for(int i=0; i<colDistance.length; i++)
			for(int j=0; j<colDistance.length; j++)
				colDistance[i][j] = 1.0f - colDistance[i][j];
		System.out.printf("Column-distance: smallest: %.3f, largest: %.3f\n", smallest, largest);

		if(connectedComp){
			System.out.println("Begin gene clustering...");
			Cluster cr = new Cluster(rowDistance);
			float dist_par = Float.parseFloat(cmd.getOptionValue("k"));
			float new_dist_par = 1.0f - (dist_par - gene_smallest) / gene_largest;
			System.out.printf("Cut-distance: %.3f\n", new_dist_par);
			TreeNode[] tr = cr.HClusterAvg(new_dist_par);
			System.out.printf("Result nodes: %d\n", tr.length);
			System.out.println("Getting components...");
			Vector<Vector<Integer> > vcr = cr.GetComponents(tr);

			for(int i=0; i<vcr.size(); i++){
				for(int j=0; j<vcr.get(i).size(); j++){
					System.out.print(vcr.get(i).get(j) + " ");
				}
				System.out.println();
			}
			/*System.out.println("Begin column clustering...");
			Cluster cc = new Cluster(colDistance);
			TreeNode[] tc = cc.HClusterAvg();
			Vector<Integer> vcc = cc.Linearize(tc);
			*/
			/*int[] row_order = new int[rowDistance.length];
			for(int i=0; i<row_order.length; i++)
				row_order[i] = vcr.get(i);

			int[] col_order = new int[colDistance.length];
			for(int i=0; i<col_order.length; i++)
				col_order[i] = vcc.get(i);
			*/
			return;
		}

		if(printOrder){
			System.out.println("Begin gene clustering...");
			Cluster cr = new Cluster(rowDistance);
			TreeNode[] tr = cr.HClusterAvg();
			Vector<Vector<Integer> > vcr = cr.Bifurcate(tr);

			System.out.println("Begin column clustering...");
			Cluster cc = new Cluster(colDistance);
			TreeNode[] tc = cc.HClusterAvg();
			Vector<Vector<Integer> > vcc = cc.Bifurcate(tc);

			System.out.printf("Gene split: %d %d\n", vcr.get(0).size(), vcr.get(1).size());
			System.out.printf("Column split: %d %d\n", vcc.get(0).size(), vcc.get(1).size());

			for(int i=0; i<vcr.size(); i++){
				System.out.printf("Gene order for branch %d:\n", (i+1));
				for(int j=0; j<vcr.get(i).size(); j++){
					System.out.printf(vcr.get(i).get(j) + " ");
				}
				System.out.println();
			}

			for(int i=0; i<vcc.size(); i++){
				System.out.printf("Column order for branch %d:\n", (i+1));
				for(int j=0; j<vcc.get(i).size(); j++){
					System.out.printf(vcc.get(i).get(j) + " ");
				}
				System.out.println();
			}
			return;
		}


		System.out.println("Begin gene clustering...");
		Cluster cr = new Cluster(rowDistance);
		TreeNode[] tr = cr.HClusterAvg();
		Vector<Integer> vcr = cr.Linearize(tr);

		System.out.println("Begin column clustering...");
		Cluster cc = new Cluster(colDistance);
		TreeNode[] tc = cc.HClusterAvg();
		Vector<Integer> vcc = cc.Linearize(tc);

		int[] row_order = new int[rowDistance.length];
		for(int i=0; i<row_order.length; i++)
			row_order[i] = vcr.get(i);

		int[] col_order = new int[colDistance.length];
		for(int i=0; i<col_order.length; i++)
			col_order[i] = vcc.get(i);

		//Write clustered matrix to file
		System.out.println("Writing results...");
		try{
			FileWriter fw = new FileWriter(output_file);
			BufferedWriter bw = new BufferedWriter(fw);
			if(containsHeader){
				bw.write("gene\t");
				for(int i=0; i<col_order.length; i++){
					bw.write(vecHeader.get(col_order[i]));
					if(i==col_order.length-1){
						bw.write("\n");
					}else{
						bw.write("\t");
					}
				}
			}

			for(int i=0; i<row_order.length; i++){
				if(containsHeader){
					bw.write(vecGenes.get(row_order[i]) + "\t");
				}
				for(int j=0; j<col_order.length; j++){
					bw.write(Integer.toString(iMatrix[row_order[i]][col_order[j]]));
					if(j==col_order.length-1){
						bw.write("\n");
					}else{
						bw.write("\t");
					}
				}
			}
			bw.close();
		}catch(IOException e){

		}

	}
}
