package seek;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.math3.distribution.HypergeometricDistribution;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.cli.HelpFormatter;
import java.io.*;
import java.util.*;
import seek.GeneSet;
import seek.Pair;

public class GeneEnrichmentTest {

	public static boolean doCommand(String cmd) throws Exception{
		try{
			Process p = Runtime.getRuntime().exec(cmd);
			BufferedReader in=new BufferedReader(new InputStreamReader(p.getInputStream())); 
			BufferedReader err=new BufferedReader(new InputStreamReader(p.getErrorStream())); 
			String s = null;
			while ((s=in.readLine())!=null){
			}
			while ((s=err.readLine())!=null){
			}
			in.close();
			err.close();
		}catch(Exception e){
			System.out.println("Error");
			return false;
		}
		return true;
	}

	public static boolean doCommand(String[] cmd) throws Exception{
		try{
			Process p = Runtime.getRuntime().exec(cmd);
			BufferedReader in=new BufferedReader(new InputStreamReader(p.getInputStream())); 
			BufferedReader err=new BufferedReader(new InputStreamReader(p.getErrorStream())); 
			String s = null;
			//System.out.println("Stdout:");
			while ((s=in.readLine())!=null){
			//	System.out.println(s);
			}
			//System.out.println("Stderr:");
			while ((s=err.readLine())!=null){
			//	System.out.println(s);
			}
			in.close();
			err.close();
		}catch(Exception e){
			System.out.println("Error");
			return false;
		}
		return true;
	}
	public static Vector<String> ReadRanks(String filename) throws IOException{
		Vector<String> ranks = new Vector<String>();
		try{
			BufferedReader in = new BufferedReader(new FileReader(filename));
			String s = null;
			int i = 0;
			while((s=in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(s, "\t");
				st.nextToken();
				ranks.add(st.nextToken());
				st.nextToken();
			}
		}catch(IOException e){
			System.out.println("Error opening file");
		}
		return ranks;
	}

	public static Vector<Pair> ReadGeneList(String filename) throws IOException{
		Vector<Pair> vp = new Vector<Pair>();
		try{
			BufferedReader in = new BufferedReader(new FileReader(filename));
			String s = null;
			int i = 0;
			Vector<String> ss = new Vector<String>();
			while((s=in.readLine())!=null){
				ss.add(s);
			}
			StringTokenizer st = new StringTokenizer(ss.get(0), " ");
			boolean oneLine = false;
			if(st.countTokens()>=3){
				oneLine = true;
			}
			if(oneLine){
				i = 0;
				while(st.hasMoreTokens()){
					vp.add(new Pair(st.nextToken(), (float) i, -1));
					i++;
				}
			}else{
				i = 0;
				for(String xs : ss){
					Pair np = new Pair(xs, (float) i, -1);
					vp.add(np);
					i++;
				}
			}
		}catch(IOException e){
			System.out.println("Error opening file");
		}
		return vp;
	}

	public static void main(String[] args) throws IOException{

		Options opt = new Options();
		opt.addOption("m", true, "Input mode and file. <0> <sessionID> (only if Tomcat6 " + 
			"environment), <1> <gscore file>, <2> <entrez gene list in text>");
		opt.addOption("l", true, "Setting directory. Specify where gene symbol/" +
			"entrez mapping, gene sets are stored. (Default: Tomcat work directory)");
		opt.addOption("t", true, "Top X genes to analyze.");
		opt.addOption("s", true, "Gene-sets to use. (msigdb_chr, targetscan, transfac, transfac_exp, chipseq_mcf7, chipseq, chipseq_geo, chipseq.50k" + 
			"piq_target, targetscan_family, original, experimental_bp_slim, withIEA, msigdb_go_bp, msigdb_go_cc, " +
			"msigdb_go_mf, msigdb_biocarta, msigdb_kegg, msigdb_reactome, msigdb_cgp, msigdb_cgp_v4)");
		opt.addOption("g", false, "Show overlapped genes.");
		opt.getOption("m").setArgs(3);
		opt.getOption("m").setRequired(true);
		opt.getOption("l").setRequired(false);
		opt.getOption("t").setRequired(true);
		opt.getOption("s").setRequired(true);

		CommandLineParser parser = new PosixParser();
		CommandLine cmd = null;
		try{
			cmd = parser.parse(opt, args);
		}catch(Exception e){
			System.out.println("Error parsing command args!");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("GeneEnrichmentTest", opt);
			return;
		}

		String tempDir = "/home/qzhu/hierarchical/enrichment";
		if(cmd.hasOption("l")){
			tempDir = new String(cmd.getOptionValue("l"));
		}
		boolean showGenes = false;
		if(cmd.hasOption("g")){
			showGenes = true;
		}

		int mode = Integer.parseInt(cmd.getOptionValues("m")[0]);
		File tempFile = null;
		if(mode==0){
			tempFile = new File(tempDir + "/" + cmd.getOptionValues("m")[1] + "_gscore");
		}else if(mode==1){
			tempFile = new File(cmd.getOptionValues("m")[1]);
		}else if(mode==2){
			tempFile = new File(cmd.getOptionValues("m")[1]);
		}else{
			System.out.println("Bad");
			return;
		}

		int top = Integer.parseInt(cmd.getOptionValue("t")); //all annotated genes in top X
		String goldstd = cmd.getOptionValue("s");
	
		//read gene entrez mapping
		String s1 = tempDir + "/" + "gene_entrez_symbol.txt";
		Map<String, Vector<String> > mge = GeneSet.readGeneEntrezMapping(s1);
		Map<String, Vector<String> > meg = GeneSet.convertGeneEntrez(mge);
		Set<String> all_entrez = meg.keySet();
		Map<Integer, String> mreverse = new HashMap<Integer, String>();
		Map<String, Integer> mentrez = GeneSet.convertGeneNameToInteger(all_entrez);
		//System.out.println("Mapping:");
		for(String s : mentrez.keySet()){
			mreverse.put(mentrez.get(s), s);
			//System.out.println(s + "\t" + mentrez.get(s));
		}

		//read gold standard gene set
		String s2 = "";
		if(goldstd.equals("msigdb_curated")){
			s2 = tempDir + "/" + "msigdb.curated.gene.set.oct11";
		}else if(goldstd.equals("msigdb_comput")){
			s2 = tempDir + "/" + "msigdb.computational.gene.set.oct11";
		}else if(goldstd.equals("msigdb_chr")){
			s2 = tempDir + "/" + "msigdb.positional.gene.set.oct11";
		}else if(goldstd.equals("netcompare")){
			s2 = tempDir + "/" + "gene_ontology_combined_experimental_netcompare.sept28";
		}else if(goldstd.equals("original")){
			//s2 = tempDir + "/" + "all_gene_ontology_annot.apr1.entrez";
			s2 = tempDir + "/" + "experimental_bp_human.annot";
		}else if(goldstd.equals("transfac")){
			s2 = tempDir + "/" + "msigdb.separate.transfac.gene.set.dec2";
		}else if(goldstd.equals("withIEA")){
			s2 = tempDir + "/" + "all_IEA_annot.entrez";
		}else if(goldstd.equals("msigdb_miRNA")){
			s2 = tempDir + "/" + "msigdb.separate.miRNA.gene.set.oct11";
		}else if(goldstd.equals("targetscan")){
			s2 = tempDir + "/" + "targetscan.conserved.entrez";
		}else if(goldstd.equals("targetscan_family")){
			s2 = tempDir + "/" + "targetscan.family.conserved.entrez";
		}else if(goldstd.equals("msigdb_go_bp")){
			s2 = tempDir + "/" + "msigdb.separate.bp.gene.set.oct11";
		}else if(goldstd.equals("msigdb_go_cc")){
			s2 = tempDir + "/" + "msigdb.separate.cc.gene.set.oct11";
		}else if(goldstd.equals("msigdb_go_mf")){
			s2 = tempDir + "/" + "msigdb.separate.mf.gene.set.oct11";
		}else if(goldstd.equals("msigdb_biocarta")){
			s2 = tempDir + "/" + "msigdb.separate.biocarta.gene.set.oct11";
		}else if(goldstd.equals("msigdb_kegg")){
			s2 = tempDir + "/" + "msigdb.separate.kegg.gene.set.oct11";
		}else if(goldstd.equals("msigdb_reactome")){
			s2 = tempDir + "/" + "msigdb.separate.reactome.gene.set.oct11";
		}else if(goldstd.equals("msigdb_cgp")){
			s2 = tempDir + "/" + "msigdb.separate.cgp.gene.set.oct11";
		}else if(goldstd.equals("msigdb_cgp_v4")){
			s2 = tempDir + "/" + "msigdb.separate.cgp.v4.gene.set.dec2";
		}else if(goldstd.equals("experimental_bp_slim")){
			s2 = tempDir + "/" + "experimental_bp_slim_human";
		}else if(goldstd.equals("transfac_exp")){
			s2 = tempDir + "/" + "transfac.experimental.dec2";
		}else if(goldstd.equals("chipseq_mcf7")){
			s2 = tempDir + "/" + "chipseq.mcf7.dec2";
		}else if(goldstd.equals("chipseq")){
			s2 = tempDir + "/" + "chipseq.all.dec2";
		}else if(goldstd.equals("chipseq.50k")){
			s2 = tempDir + "/" + "gene_set_chipseq_feb14.entrez.txt";
		}else if(goldstd.equals("chipseq_geo")){
			s2 = tempDir + "/" + "chipseq.geo.dec2";
		}else if(goldstd.equals("piq_target")){
			s2 = tempDir + "/" + "piq.targets.dec15";
		}

		//String[] genes = (String[]) vecGenes.toArray();

		//String[] genes = {"SMO", "CCDC8", "MYOZ3", "PCDH18", "HEYL", "SEMA6A", "EDA", 
		//				"CRMP1", "PTCH2", "PCYT1B", "SDK2", "SYDE1", "PKDCC", "ARVCF",
		//				"IHH", "DTX3", "DCHS1", "HHIP", "NFATC4", "CPZ", "HHIP-AS1"};

		Map<String, Vector<String> > gold_std = GeneSet.readGeneSet(s2);
		Map<String, Vector<Integer> > gold_std_int = GeneSet.convertGeneSetToInteger(gold_std, mentrez);
		int population = 18000; //temporary
		Set<Integer> all_genes = new HashSet<Integer>();
		Set<String> all_gene_names = new HashSet<String>();
		for(String term : gold_std_int.keySet()){
			for(Integer gs : gold_std_int.get(term))
				all_genes.add(gs);
			for(String gg : gold_std.get(term))
				all_gene_names.add(gg);
		}
		population = all_genes.size();

		//reading gene names sorted by gene scores
		//File tempFile = new File(tempDir + "/" + sessionID);
		String name_mapping = tempDir + "/gene_id.map";
		Map<String, Integer> mm = ReadScore.readGeneMapping(name_mapping);		
		//System.out.println(tempFile.getAbsolutePath());
		Vector<Pair> valid = null;
		if(mode==0 || mode==1){
			float[] sc = ReadScore.ReadScoreBinary(tempFile.getAbsolutePath());
			valid = ReadScore.SortScores(mm, sc);
		}else if(mode==2){
			valid = ReadGeneList(tempFile.getAbsolutePath());
		}

		//System.out.println(sc.length + " " + valid.size());
		//get top X genes (where X is a parameter from command line)
		Vector<Integer> query = new Vector<Integer>();
		if(top>valid.size()){
			System.out.println("Top exceed gene list size, replace with max size.");
			top = valid.size();
		}

		for(int i=0; i<top; i++){
			if(all_gene_names.contains(valid.get(i).term)){
				if(mentrez.containsKey(valid.get(i).term)) 
					query.add(mentrez.get(valid.get(i).term));
				//System.out.println(valid.get(i).term + "\t" + mentrez.get(valid.get(i).term));
			}
		}
		Collections.sort(query);
		System.out.printf("Pop: %d/%d Q: %d/%d\n", population, valid.size(), 
			query.size(), top);
		
		Vector<Pair> vp = new Vector<Pair>();
		Map<String, Vector<Integer> > map_overlap = new HashMap<String, Vector<Integer> >();
		for(String term : gold_std_int.keySet()){
			Vector<Integer> overlap = GeneSet.getGeneSetOverlapGenes(query, gold_std_int.get(term));
			//System.out.println(term);
			//if(overlap.size()<=1) continue;
			map_overlap.put(term, new Vector<Integer>(overlap));
			int successes = gold_std_int.get(term).size();
			HypergeometricDistribution hyp = new HypergeometricDistribution(population, successes, query.size());
			double prob = hyp.upperCumulativeProbability(overlap.size());
			if(prob>=1.0){
				prob = 0.999999999;
			}	
			Pair pp = new Pair(term, prob, -1);
			vp.add(pp);
		}	
		Collections.sort(vp);

		/*for(Pair p : vp){
			System.out.println(p.term + "\t" + p.val);
		}*/		
		//String st = tempDir + "/" + sessionID + ".pval";
		String st = "/tmp/" + tempFile.getName() + ".pval";
		PrintWriter out1 = null;
		try{
			out1 = new PrintWriter(new FileWriter(st));
			for(int i=0; i<vp.size(); i++){
				Pair pi = vp.get(i);
				out1.println(pi.val);
				//System.out.println(pi.val);
			}
			//System.out.println("Done");
		}catch(Exception e){
			System.out.println("BAD IO 1");
			//e.printStackTrace();
			return ;
		}finally{
			if(out1 !=null) out1.close();
		}
		
		//String sv = tempDir + "/" + sessionID + ".qval";
		String sv = "/tmp/" + tempFile.getName() + ".qval";
		try{
			if(!doCommand(tempDir + "/qval.R " + st + " " + sv)){
				System.out.println("Error!");
				return ;
			}
		}catch(Exception e){
			System.out.println("exception happened - here's what I know: ");
			e.printStackTrace();
		}

		Vector<Float> qval = new Vector<Float>();
		BufferedReader in = null;
		try{
			in = new BufferedReader(new FileReader(sv));
			String s = null;
			while((s=in.readLine())!=null){
				qval.add(Float.parseFloat(s));
			}
		}catch(IOException e){
			System.out.println("BAD IO 2");
			return ;
		}finally{
			if(in!=null) in.close();
		}

		float prev_value = 0;
		for(int i=0; i<vp.size(); i++){
			Pair pi = vp.get(i);
			float b_value = (float) pi.val * (float) gold_std_int.get(pi.term).size() / (float) (i+1);
			if(b_value>1f){
				b_value = 1.0f;
			}
			if(b_value<prev_value){
				b_value = prev_value;
			}
			prev_value = b_value;

			if(goldstd.equals("targetscan") || goldstd.equals("targetscan_family")){
				if(map_overlap.get(pi.term).size()<=1) continue;
				if(pi.val>=0.05 || qval.get(i)>0.5) continue;
				System.out.printf("%s\t%.1E\t%.1E\t%d\t%d\n", pi.term, b_value, qval.get(i),
					gold_std_int.get(pi.term).size(), map_overlap.get(pi.term).size());

				//System.out.println(pi.term + "\t" + b_value + "\t" + qval.get(i) +"\t" + 
				//	gold_std_int.get(pi.term).size() + "\t" + 
					//query.size() + "\t" + 
				//	map_overlap.get(pi.term).size());
				/*for(Integer ii : map_overlap.get(pi.term))
					for(String ss : meg.get(mreverse.get(ii)))
						System.out.print(ss + " ");		
				System.out.println();
				*/
			}else{
				//if(!geneSets.contains(pi.term)){
				//	continue;
				//}
				if(pi.val>=0.05 || qval.get(i)>0.25){
					break;
				}
				System.out.printf("%s\t%.1E\t%.1E\t%d\t%d\n", pi.term, b_value, qval.get(i),
					gold_std_int.get(pi.term).size(), map_overlap.get(pi.term).size());
				//System.out.println(pi.term + "\t" + b_value + "\t" + qval.get(i) + "\t" 
				//	+ gold_std_int.get(pi.term).size() + "\t" +
					//query.size() + "\t" + 
				//	+ map_overlap.get(pi.term).size());
			}
			if(showGenes){
				Vector<String> all_to_print = new Vector<String>();
				for(Integer ii : map_overlap.get(pi.term))
					//print entrez gene id
					for(String ss : meg.get(mreverse.get(ii))){
					//for(String ss : mreverse.get(ii)){
						all_to_print.add(ss);
						//System.out.print(ss + " ");
						//System.out.print(mreverse.get(ii) + " ");
						//System.out.print(ii + " ");
					}				
					//System.out.print(mreverse.get(ii) + " ");
				Collections.sort(all_to_print);
				for(String ss : all_to_print){
					System.out.print(ss + " ");
				}
				System.out.println();
			}

		}
	}
}
